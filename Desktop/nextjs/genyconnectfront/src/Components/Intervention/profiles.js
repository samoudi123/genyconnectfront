import '../Intervention/profiles.css';
import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link, useParams } from 'react-router-dom';
import $ from 'jquery';

function Profiles(props) {
  const [technicians, setTechnicians] = useState([]);
  const { id } = useParams();
  console.log(id);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(`http://localhost:8080/api/technicians/${id}/scored`);
        setTechnicians(response.data);
        console.log(response);
      } catch (error) {
        console.error("Error fetching technicians:", error);
      }
    };

    fetchData();
  }, [id]);

  useEffect(() => {
    $(document).ready(function() {
      $('.profile-initials').each(function() {
        const firstName = $(this).data('firstname');
        const lastName = $(this).data('lastname');
        const initials = (firstName ? firstName.charAt(0) : '') + (lastName ? lastName.charAt(0) : '');
        $(this).text(initials);
      });
    });
  }, [technicians]);

  return (
    <div className="container mt-5 mb-3">
      <div className="row">
        {technicians.map((technician) => (
          <div className="col-md-4" key={technician.id}>
            <div className="card p-3 mb-2">
              <div className="d-flex justify-content-between">
                <div className="d-flex flex-row align-items-center">
                  <div className="img-holder mr-md-4 mb-md-0 mb-4 mx-auto mx-md-0 d-md-none d-lg-flex profile-initials"
                    data-firstname={technician.firstname}
                    data-lastname={technician.lastname}>
                  </div> 
                  <div className="ms-2 c-details">
                    <h6 className="mb-0">{technician.firstname} {technician.lastname}</h6> <span>{technician.combinedScore}</span>
                  </div>
                </div>
                <Link to={{ pathname: `/profile/${technician.id}/${id}` }}>
                  <button className="btn btn-outline-primary px-4">view profile</button>

                </Link>
              </div>
              <div className="mt-5"></div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}

export default Profiles;
