import React, { useRef } from 'react';
import '../Intervention/fiche.css';
import { useLocation, useNavigate } from 'react-router-dom';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

function FicheIntervention() {
    const pdfRef = useRef();
    const navigate = useNavigate();
    const location = useLocation();
    const { 
        marque, 
        referenceDevice, 
        typeAppareil, 
        dateAcquisition, 
        dateService, 
        adress, 
        numeroSerie, 
        disinfectedEquipment, 
        additionnalComments 
    } = location.state || {};

    const downloadPDF = () => {
        const input = pdfRef.current;
        html2canvas(input).then((canvas) => {
            const imgData = canvas.toDataURL('image/png');
            const pdf = new jsPDF('p', 'mm', 'a4', true);
            const pdfWidth = pdf.internal.pageSize.getWidth();
            const pdfHeight = pdf.internal.pageSize.getHeight();
            const imgWidth = canvas.width;
            const imgHeight = canvas.height;
            const ratio = Math.min(pdfWidth / imgWidth, pdfHeight / imgHeight);
            const imgX = (pdfWidth - imgWidth * ratio) / 2;
            const imgY = 30;
            pdf.addImage(imgData, 'PNG', imgX, imgY, imgWidth * ratio, imgHeight * ratio);
            pdf.save('fiche.pdf');
            navigate(-2);
        });
    };

    return (
        <>
            <div ref={pdfRef} className="fiche">
                <div className="word-document">
                    <div className="title-section">
                        <h1>Intervention request form for biomedical equipment</h1>
                    </div>
                    <p className='pp'> Requesting agency: ......................................... Date of demand: ........................................................</p>
                    <div className="section">
                        <h5 className='h55'>1. Device: </h5>
                        <div className="info-row">
                            <h6>Brand:</h6>
                            <p>{marque}</p>
                        </div>
                        <div className="info-row">
                            <h6>Address:</h6>
                            <p>{adress}</p>
                        </div>
                        <div className="info-row">
                            <h6>Reference:</h6>
                            <p>{referenceDevice}</p>
                        </div>
                        <div className="info-row">
                            <h6>Type:</h6>
                            <p>{typeAppareil}</p>
                        </div>
                        <div className="info-row">
                            <h6>Date of Commissioning:</h6>
                            <p>{dateService}</p>
                        </div>
                        <div className="info-row">
                            <h6>Acquisition Date:</h6>
                            <p>{dateAcquisition}</p>
                        </div>
                        <div className="info-row">
                            <h6>Serial Number:</h6>
                            <p>{numeroSerie}</p>
                        </div>
                        <div className="info-row">
                            <h6>Model:</h6>
                            <p>{disinfectedEquipment}</p>
                        </div>
                    </div>
                    <div className="section1">
                        <h5 className='h55'>2. Description of the problem: </h5>
                        <div>
                            <input type="checkbox" id="exampleUniq1" />
                            <label htmlFor="exampleUniq1">Total breakdown</label>
                        </div>
                        <div>
                            <input type="checkbox" id="exampleUniq2" />
                            <label htmlFor="exampleUniq2">Power or battery problem</label>
                        </div>
                        <div>
                            <input type="checkbox" id="exampleUniq3" />
                            <label htmlFor="exampleUniq3">False contacts</label>
                        </div>
                        <div>
                            <input type="checkbox" id="exampleUniq4" />
                            <label htmlFor="exampleUniq4">fall Accessory fault</label>
                        </div>
                        <div>
                            <input type="checkbox" id="exampleUniq5" />
                            <label htmlFor="exampleUniq5">Intermittent failure</label>
                        </div>
                        <div>
                            <input type="checkbox" id="exampleUniq6" />
                            <label htmlFor="exampleUniq6">Faulty settings</label>
                        </div>
                        <div>
                            <input type="checkbox" id="exampleUniq7" />
                            <label htmlFor="exampleUniq7">Breakage, fall</label>
                        </div>
                    </div>
                    <div className="section2">
                        <h5 className='h55'>3. Additional comments: </h5>
                        <p>{additionnalComments}</p>
                    </div>
                    <p className='pp'>Name of person reporting the defect: ...................</p>
                    <p className='pp'>Phone Number: ...................</p>
                    <p className='pp'>Signature of Applicant: .............</p>
                </div>
            </div>
            <button type='submit' className="btn btn-primary" onClick={downloadPDF}>Print</button>
        </>
    );
}

export default FicheIntervention;
