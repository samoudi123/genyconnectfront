import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link, useParams } from 'react-router-dom';
import SideBar1 from "../profile/SideBar";
import intervention from "../Assets/inter.png";

function Intervention() {
    const { userId } = useParams();
    const [userData, setUserData] = useState([]);
    const [uploadedSheets, setUploadedSheets] = useState({}); 

    const Endpoint = `http://localhost:8080/api/request/client/${userId}`;

    const getUserData = async () => {
        try {
            const fetchData = await axios.get(Endpoint);
            setUserData(fetchData.data);
            console.log(typeof userData); 

       

        } catch (error) {
            console.log(error);
        }
    };


    useEffect(() => {
        getUserData();
    }, [userId]);

    const handleUploadSuccess = (interventionId) => {
        setUploadedSheets(prevState => ({
            ...prevState,
            [interventionId]: true
        }));
    };

    const buttonContainerStyle = {
        display: 'flex',
        justifyContent: 'flex-end',
        marginTop: '20px',
    };

    const customButtonStyle = {
        backgroundColor: '#176184',
        fontWeight: 'bold',
        color: 'white',
        padding: '10px 20px',
        border: 'none',
        borderRadius: '5px',
        cursor: 'pointer',
    };

    const table = {
        color: '#0DA2B4',
    };

    const cssStyles = `
        .emp-profile {
            padding: 3%;
            margin-top: 3%;
            margin-bottom: 3%;
            border-radius: 0.5rem;
            background: #fff;
        }
        .profile-img {
            text-align: center;
        }
        .profile-img img {
            width: 70%;
            height: 100%;
        }
        .profile-img .file {
            position: relative;
            overflow: hidden;
            margin-top: -20%;
            width: 70%;
            border: none;
            border-radius: 0;
            font-size: 15px;
            background: #212529b8;
        }
        .profile-img .file input {
            position: absolute;
            opacity: 0;
            right: 0;
            top: 0;
        }
        .profile-head h5 {
            color: #333;
        }
        .profile-head h6 {
            color: #0062cc;
        }
        .profile-edit-btn {
            border: none;
            border-radius: 1.5rem;
            width: 70%;
            padding: 2%;
            font-weight: 600;
            color: #6c757d;
            cursor: pointer;
        }
        .proile-rating {
            font-size: 12px;
            color: #818182;
            margin-top: 5%;
        }
        .proile-rating span {
            color: #495057;
            font-size: 15px;
            font-weight: 600;
        }
        .profile-head .nav-tabs {
            margin-bottom: 5%;
        }
        .profile-head .nav-tabs .nav-link {
            font-weight: 600;
            border: none;
        }
        .profile-head .nav-tabs .nav-link.active {
            border: none;
            border-bottom: 2px solid #0062cc;
        }
        .profile-work {
            padding: 14%;
            margin-top: -15%;
        }
        .profile-work p {
            font-size: 12px;
            color: #818182;
            font-weight: 600;
            margin-top: 10%;
        }
        .profile-work a {
            text-decoration: none;
            color: #495057;
            font-weight: 600;
            font-size: 14px;
        }
        .profile-work ul {
            list-style: none;
        }
        .profile-tab label {
            font-weight: 600;
        }
        .profile-tab p {
            font-weight: 600;
            color: #0062cc;
        }
        body, html {
            margin: 10;
            padding: 10;
        }
        .app-container {
            width: 100%;
            max-width: none;
            margin: 0;
            padding: 0;
        }
        .navbar {
            width: 93%;
            position: fixed;
            top: 0;
            z-index: 1000;
        }
    `;
    
    const listItemStyle = {
        marginTop: '60px',
    };

    return (
        <div className="container">
            <style>{cssStyles}</style>
            <nav className="navbar navbar-expand-xl navbar-blue bg-blue">
                <div className="container-fluid">
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav me-auto mb-2 mb-lg-0"></ul>
                        <form className="d-flex justify-content-end">
                            <Link to="/chat">Conversations</Link>
                            <span style={{ margin: '0 10px' }}></span>
                            <Link to="/calls">Calls</Link>
                            <span style={{ margin: '0 10px' }}></span>
                        </form>
                    </div>
                </div>
            </nav>
            <div className="row">
                <div className="col-md-2">
                    <SideBar1 userId={userId} />
                </div>
                <div className="col-md-10">
                    {userData && userData.map((item) => (
                        <li className="card p-2 mb-2" key={item.id} style={listItemStyle}>
                            <div className="coard-body">
                                <p className="card-text d-flex justify-content-between align-items-center">
                                    <Link to={`/details/${item.reference}`}>
                                    <img src={intervention} alt="Intervention Icon" style={{ width: '40px', marginRight: '10px', filter: 'invert(26%) sepia(73%) saturate(6545%) hue-rotate(185deg) brightness(93%) contrast(102%)' }} />

                                        {item.reference}
</Link>
                                    <div style={table}>
                                        <Link to={`/uploadfiche/${item.id}`}>
                                            <button
                                                type="button"
                                                className="btn btn-outline-success m-2"
                                                disabled={!!item.fileName1}

                                                onClick={() => handleUploadSuccess(item.id)}
                                            >
                                                Upload intervention sheet
                                            </button>
                                        </Link>
                                        <Link to={`/timeline/${item.reference}`}>
                                            <button type="button" className="btn btn-outline-secondary m-2" disabled={item.status === 'terminé' || (item.status !== 'en_cours'   || !item.fileName1 || !item.fileName)}>In progress</button>
                                        </Link>
                                        <Link to={`/profiles/${item.id}`}>
                                        <button type="button" className="btn btn-outline-info" 
        disabled={item.status === 'en_cours' || (item.status !== 'en_attente' || !item.fileName1)}>
  Tech Suggestions
</button>
                                        </Link>
                                    </div>
                                </p>
                            </div>
                        </li>
                    ))}
                    <div style={buttonContainerStyle}>
                        <Link to={`/interventionForm/${userId}`}>
                            <button style={customButtonStyle}>Create Intervention</button>
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Intervention;