import React, { useRef } from 'react';
import '../Intervention/fiche.css'
import { useLocation, useParams } from 'react-router-dom';
import jsPDF from 'jspdf';
import { useNavigate } from 'react-router-dom'; 

import html2canvas from 'html2canvas';
function FicheDiagnostic() {
    const pdfRef = useRef();
    const navigate = useNavigate(); 
    const location = useLocation();
    const queryParams = new URLSearchParams(location.search);

const downloadPDF=()=>{
    const input = pdfRef.current;
    html2canvas(input).then((canvas) => { 
        const imgData = canvas.toDataURL('image/png');
        const pdf = new jsPDF('p','mm','a4',true);
        const pdfWidth= pdf.internal.pageSize.getWidth();
        const  pdfHeight = pdf.internal.pageSize.getHeight();
        const imgWidth = canvas.width;
        const imgHeight = canvas.height;
        const ratio = Math.min(pdfWidth / imgWidth,pdfHeight/imgHeight);
        const imgX = (pdfWidth - imgWidth * ratio)/2;
        const imgY = 30;
        pdf.addImage(imgData, 'PNG', imgX, imgY, imgWidth*ratio, imgHeight*ratio); 
        pdf.save('fiche.pdf'); 
           // Une fois le téléchargement terminé, naviguer vers /intervention/${userId}
           const userId = queryParams.get('userId');
           console.log(userId);
           navigate(-3); 
});
}
const tableStyle = {
    borderCollapse: 'collapse',
    width: '100%',
  };
  
  const thTdStyle = {
    border: '1px solid #dddddd',
    textAlign: 'left',
    padding: '8px',
  };
  
  const thStyle = {
    ...thTdStyle,
    backgroundColor: '#f2f2f2',
  };

return(
    <><div ref={pdfRef} className="fiche">

        <div className="word-document">
            <div className="title-section">
                <h1>diagnostic sheet for biomedical equipment </h1>
            </div>
            <p className='pp'> technician name: .........................................  Date of diagnosis: ........................................................</p>
            <div className="section">
                <h5 className='h55'>1.equipment concerned: </h5>

                <p></p>
                <p></p>

            </div>
            <p></p>
            
            <p></p>
            <div className="section1">

                <h5 className='h55'>2.Description of the problem: </h5>
                <p>{queryParams.get('problems')}</p>
               
            </div>
            <p></p>
            <div className="section2">
                <h5 className='h55'>3.defects noted: </h5>
                <p>{queryParams.get('causes')}</p>

            </div>
            <p></p>
            <div className="section">

                <h6>Cost:</h6>
                <p>{queryParams.get('cost')}</p>
                <h6>Duration:</h6>
                <p>{queryParams.get('duration')}</p>

                
                
                
            </div>
            <p></p>
<h5>Spare parts:</h5>
            <table style={tableStyle} className="table">
      <thead>
        <tr>
          <th style={thStyle}>Name</th>
          <th style={thStyle}>Code</th>
          <th style={thStyle}>Series</th>
          <th style={thStyle}>Number</th>
          <th style={thStyle}>Price</th>

        </tr>
      </thead>
      <tbody>
        <tr>
          <td style={thTdStyle}></td>
          <td style={thTdStyle}></td>
          <td style={thTdStyle}></td>
          <td style={thTdStyle}></td>
          <td style={thTdStyle}></td>

        </tr>
        <tr>
          <td style={thTdStyle}></td>
          <td style={thTdStyle}></td>
          <td style={thTdStyle}></td>
          <td style={thTdStyle}></td>
          <td style={thTdStyle}></td>

        </tr>
        <tr>
          <td style={thTdStyle}></td>
          <td style={thTdStyle}></td>
          <td style={thTdStyle}></td>
          <td style={thTdStyle}></td>
          <td style={thTdStyle}></td>

        </tr>
        <tr>
          <td style={thTdStyle}></td>
          <td style={thTdStyle}></td>
          <td style={thTdStyle}></td>
          <td style={thTdStyle}></td>
          <td style={thTdStyle}></td>

        </tr>
        <tr>
          <td style={thTdStyle}></td>
          <td style={thTdStyle}></td>
          <td style={thTdStyle}></td>
          <td style={thTdStyle}></td>
          <td style={thTdStyle}></td>

        </tr>
        <tr>
          <td style={thTdStyle}></td>
          <td style={thTdStyle}></td>
          <td style={thTdStyle}></td>
          <td style={thTdStyle}></td>
          <td style={thTdStyle}></td>

        </tr>
        <tr>
          <td style={thTdStyle}></td>
          <td style={thTdStyle}></td>
          <td style={thTdStyle}></td>
          <td style={thTdStyle}></td>
          <td style={thTdStyle}></td>

        </tr>
        <tr>
          <td style={thTdStyle}></td>
          <td style={thTdStyle}></td>
          <td style={thTdStyle}></td>
          <td style={thTdStyle}></td>
          <td style={thTdStyle}></td>

        </tr>

      </tbody>
    </table>

            <p className='pp'>Phone Number: ...................
            </p>
            <p className='pp'>Signature of Applicant
                : .............</p>
        </div>
    </div>
    <button type='submit' className="btn btn-primary" onClick={downloadPDF}> Print</button></>

);



}
export default FicheDiagnostic;