import '../Intervention/profile.css'
import { useState } from 'react';
import $ from 'jquery';

import { useParams, useNavigate, Link } from 'react-router-dom';
import React, { useEffect } from 'react';


function Profile(){
    const navigate = useNavigate();

    const [userProfile, setUserProfile] = useState({});
    const {userId} = useParams(); // Utilisez useParams pour récupérer l'userId de l'URL
  
    const {  id } = useParams(); // Utilisez useParams pour récupérer userId et id de l'URL


console.log(userId);
    useEffect(() => {
        fetchUserProfile();
    }, []);
   
    const fetchUserProfile = async () => {
        try {
            const response = await fetch(`http://localhost:8080/api/v1/users/${userId}`, {
                method: 'GET', 
            });
            if (!response.ok) {
                throw new Error('Erreur lors de la récupération du profil utilisateur');
            }
            const userData = await response.json();
            setUserProfile(userData);
        } catch (error) {
            console.error('Erreur:', error);
            // Gérer les erreurs de récupération de profil ici
        }
    };

    // Affichez les données du profil une fois qu'elles sont récupérées
    console.log(userProfile);
    console.log(userId);
    console.log(id);

   

    $(document).ready(function(){
        var firstName = $('#firstName').text();
        var lastName = $('#lastName').text();
        var intials = firstName.charAt(0) + lastName.charAt(0);
        var profileImage = $('#profileImage').text(intials);
      });


    

    const assignToRequest = async () => {
        try {
            if (id) {
                const response = await fetch(`http://localhost:8080/api/request/assignTechnician/${id}/${userId}`, {
                    method: 'POST',
                });
                if (!response.ok) {
                    throw new Error('Erreur lors de l\'assignation à la demande');
                }
                // Traitez la réponse si nécessaire
                console.log('Assigné avec succès à la demande');
                navigate(-2);


            } else {
                console.error('Erreur: requestId est null');
            }
        } catch (error) {
            console.error('Erreur:', error);
            // Gérer les erreurs d'assignation ici
        }
    };
    
return(


<div className="container mt-5">
    
    <div className="row d-flex justify-content-center">
        
        <div className="col-md-7">
            
            <div className="card p-3 py-4">
                
                <div className="text-center">
                <span id="firstName" style={{ display: 'none' }}>{userProfile.firstname ? userProfile.firstname.charAt(0).toUpperCase() : ''}</span>

               <div id="profileImage"></div>
                </div>
                
                <div className="text-center mt-3">
                    <h5 className="mt-2 mb-0">{userProfile.firstname} {userProfile.lastname} </h5>
                    <span>{userProfile.adress ? userProfile.adress : ''}</span>
                    
                    <h6 className="mt-2 mb-0">{userProfile.diplome} </h6>
                    <span>{userProfile.specialityy}</span>
                    <h6 className="mt-2 mb-0">{userProfile.nbreIntervention} intervention</h6>
                    <h6 className="mt-2 mb-0">{userProfile.experience} years of experience</h6>

                    
                    
                     <ul className="social-list">
                        <li><i className="fa fa-facebook"></i></li>
                        <li><i className="fa fa-dribbble"></i></li>
                        <li><i className="fa fa-instagram"></i></li>
                        <li><i className="fa fa-linkedin"></i></li>
                        <li><i className="fa fa-google"></i></li>
                    </ul>
                    
                    <div className="buttons">
                    
                        <button className="btn btn-primary px-4 ms-3" onClick={assignToRequest}>Assign to request</button>

                    </div>
                    
                    
                </div>
                
               
                
                
            </div>
            
        </div>
        
    </div>
    
</div>
);

}
export default Profile;
