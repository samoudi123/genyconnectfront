import React, { useState } from 'react';
import SideBar from "../profile/SideBar";
import "react-datepicker/dist/react-datepicker.css";
import axios from 'axios';
import { useParams, useNavigate } from 'react-router-dom'; 

const InterventionForm = () => {
    const { userId } = useParams();
    const navigate = useNavigate(); 

    const [typeAppareil, setTypeAppareil] = useState('');
    const [marque, setMarque] = useState('');
    const [referenceDevice, setReferenceDevice] = useState('');
    const [disinfectedEquipment, setDisinfectedEquipment] = useState('');
    const [adress, setAdress] = useState('');
    const [numeroSerie, setNumeroSerie] = useState('');
    const [dateAcquisition, setDateAcquisition] = useState('');
    const [dateService, setDateService] = useState('');
    const [additionnalComments, setAddictionnalComments] = useState('');

    const saveForm = async (event) => {
        event.preventDefault();
        
        if (!typeAppareil || !marque || !referenceDevice || !disinfectedEquipment || !adress || !numeroSerie || !dateAcquisition || !dateService ) {
            alert("Please fill in all fields.");
            return;
        }

        // Log des données avant l'envoi
        console.log({
            typeAppareil,
            marque,
            referenceDevice,
            disinfectedEquipment,
            adress,
            numeroSerie,
            dateAcquisition,
            dateService,
            additionnalComments
        });

        try {
            await axios.post(`http://localhost:8080/api/request/${userId}`, {
                typeAppareil,
                marque,
                referenceDevice,
                disinfectedEquipment,
                adress,
                numeroSerie,
                dateAcquisition,
                dateService,
                additionnalComments
            });

            alert('Successfull saving');
            navigate('/fiche', {
                state: {
                    marque,
                    referenceDevice,
                    typeAppareil,
                    dateAcquisition,
                    dateService,
                    adress,
                    numeroSerie,
                    disinfectedEquipment,
                    additionnalComments
                }
            });
            
        } catch (error) {
            alert(error);
        }
    };

    return (
        <>
            <SideBar />
            <div style={{ marginLeft: '230px', marginTop: '5px' }}>
                <form>
                    <div className="row">
                        <div className="col-md-5 mb-3">
                            <label className="form-label">Brand</label>
                            <input type="text" className="form-control" placeholder='brand' value={marque} onChange={(e) => setMarque(e.target.value)} required />
                        </div>
                        <div className="col-md-5 mb-3">
                            <label className="form-label">Device Reference</label>
                            <input type="text" className="form-control" placeholder='Device Reference' value={referenceDevice} onChange={(e) => setReferenceDevice(e.target.value)} required />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-5 mb-3">
                            <label className="form-label">Acquisition Date</label>
                            <input type="date" className="form-control" placeholder='YYY-MM-JJ' value={dateAcquisition} onChange={(e) => setDateAcquisition(e.target.value)} required />
                        </div>
                        <div className="col-md-5 mb-3">
                            <label className="form-label">Date of Commissioning</label>
                            <input type="date" className="form-control" placeholder='YYY-MM-JJ' value={dateService} onChange={(e) => setDateService(e.target.value)} required />
                        </div>
                        <div className="col-md-5 mb-3">
                            <label className="form-label">Address</label>
                            <input type="text" className="form-control" placeholder='Adress' value={adress} onChange={(e) => setAdress(e.target.value)} required />
                        </div>
                        <div className="col-md-5 mb-3">
                            <label className="form-label">Serial Number</label>
                            <input type="text" className="form-control" placeholder='Serial Number' value={numeroSerie} onChange={(e) => setNumeroSerie(e.target.value)} required />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-5 mb-3">
                            <label className="form-label">Device Type</label>
                            <select className="form-select" value={typeAppareil} onChange={(e) => setTypeAppareil(e.target.value)} required>
                                <option selected>Choose...</option>
                                <option>Scanner</option>
                                <option>Respirator</option>
                                <option>Ultrasound</option>
                                <option>Centrifugal</option>
                                <option>Spectrophotometer</option>
                            </select>
                        </div>
                        <div className="col-md-5 mb-3">
                            <label className="form-label">Model</label>
                            <input className="form-control" placeholder='Model' value={disinfectedEquipment} onChange={(e) => setDisinfectedEquipment(e.target.value)} required />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-10 mb-3">
                            <label className="form-label">Additional Comments</label>
                            <textarea className="form-control" style={{ height: '100px' }} value={additionnalComments} onChange={(e) => setAddictionnalComments(e.target.value)} required />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-4">
                            <button type="submit" className="btn btn-primary" onClick={saveForm}>Add</button>
                        </div>
                    </div>
                </form>
            </div>
        </>
    );
}

export default InterventionForm;
