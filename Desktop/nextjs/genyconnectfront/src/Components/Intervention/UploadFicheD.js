import React, { useState, useEffect } from 'react';
import { Viewer } from '@react-pdf-viewer/core';
import { defaultLayoutPlugin } from '@react-pdf-viewer/default-layout';
import '@react-pdf-viewer/core/lib/styles/index.css';
import '@react-pdf-viewer/default-layout/lib/styles/index.css';
import { Worker } from '@react-pdf-viewer/core';
import { useNavigate, useParams } from 'react-router-dom';

function UploadFiche() {
    const [uploadButtonDisabled, setUploadButtonDisabled] = useState(true); // Initialise le bouton désactivé

    const navigate = useNavigate();
    const { id } = useParams();
    const defaultLayoutPluginInstance = defaultLayoutPlugin();
    const [file, setFile] = useState(null);
    const [viewPdf, setViewPdf] = useState(null);
    const [uploadedPdf, setUploadedPdf] = useState(null); // État pour stocker le PDF téléchargé

    const fileType = ['application/pdf'];

    useEffect(() => {
        console.log("uploadedPdf:", uploadedPdf);
    }, [uploadedPdf]);

    const handleChange = (e) => {
        const selectedFile = e.target.files[0];
        if (selectedFile && fileType.includes(selectedFile.type)) {
            setFile(selectedFile);
        } else {
            setFile(null);
            console.log('Please select a valid PDF file.');
        }
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        if (file) {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = (e) => {
                setViewPdf(e.target.result);
            };
        } else {
            setViewPdf(null);
        }
    };

    const handleSubmit1 = async (e) => {
        e.preventDefault();
        if (file !== null) {
            try {
                const formData = new FormData();
                formData.append('file', file);

                const response = await fetch(`http://localhost:8080/api/request/upload/${id}`, {
                    method: 'POST',
                    body: formData,
                });

                if (response.ok) {
                    console.log('File uploaded successfully');
                    setUploadedPdf(file); 
                    navigate(-1);
                } else {
                    console.error('Failed to upload file');
                }
            } catch (error) {
                console.error('Error uploading file:', error);
            }
        } else {
            console.log('No file selected');
        }
    };
    useEffect(() => {
        async function fetchData() {
            try {
                const response = await fetch(`http://localhost:8080/api/request/${id}`);
                if (response.ok) {
                    const data = await response.json();
                    // Vérifiez si fileUrl1 et fileName1 sont vides
                    if (!data.fileUrl || !data.fileName) {
                        setUploadButtonDisabled(false); // Active le bouton si fileUrl1 ou fileName1 est vide
                    }
                } else {
                    console.error('Failed to fetch data');
                }
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }

        fetchData();
    }, [id]);

    return (
        <div className='container'>
            <form encType='multipart/form-data' onSubmit={handleSubmit}>
                <input type='file' name='file' className='form-control' onChange={handleChange} />
                <button type='submit' className='btn btn-success'>
                    View pdf
                </button>
                <p></p>
                <button type='button' className='btn btn-success' onClick={handleSubmit1} le disabled={uploadButtonDisabled}>
                    Upload pdf
                </button>
            </form>
            <div className='pdf-container'>
                <Worker workerUrl='https://unpkg.com/pdfjs-dist@2.15.349/build/pdf.worker.min.js'>
                    {viewPdf ? (
                        <Viewer fileUrl={viewPdf} plugins={[defaultLayoutPluginInstance]} />
                    ) : (
                        <>no pdf</>
                    )}
                </Worker>
            </div>
        </div>
    );
}

export default UploadFiche;
