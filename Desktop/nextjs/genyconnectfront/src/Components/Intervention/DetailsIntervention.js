import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import axios from "axios";

function DetailsIntervention() {
    const { reference } = useParams();
    const [requestDetails, setRequestDetails] = useState({});
    const navigate = useNavigate();

    useEffect(() => {
        const getRequestDetails = async () => {
            try {
                const response = await axios.get(`http://localhost:8080/api/request/reference/${reference}`);
                setRequestDetails(response.data);
                console.log(requestDetails);
            } catch (error) {
                console.error("Error fetching request details:", error);
            }
        };

        getRequestDetails();
    }, [reference]);

    const cancelRequest = async () => {
        try {
            await axios.post(`http://localhost:8080/api/request/cancelAssignment/${reference}`);
            navigate(-1);
        } catch (error) {
            console.error("Error cancelling request:", error);
        }
    };

    const handleDownloadClick1 = async () => {
        try {
            const response = await axios.get(`http://localhost:8080/api/request/download1/${reference}`, {
                responseType: 'blob',
            });
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', 'file.pdf');
            document.body.appendChild(link);
            link.click();
        } catch (error) {
            console.error("Error downloading file:", error);
        }
    };

    const handleDownloadClick = async () => {
        try {
            const response = await axios.get(`http://localhost:8080/api/request/download/${reference}`, {
                responseType: 'blob',
            });
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', 'file.pdf');
            document.body.appendChild(link);
            link.click();
        } catch (error) {
            console.error("Error downloading file:", error);
        }
    };

    return (
        <div className="container mt-4">
            <h1 className="mb-4">Maintenance Request Details</h1>
            <table className="table">
                <tbody>
                    <tr>
                        <th>Device Type</th>
                        <td>{requestDetails[0] && requestDetails[0].typeAppareil}</td>
                        <th>Brand</th>
                        <td>{requestDetails[0] && requestDetails[0].marque}</td>
                    </tr>
                    <tr>
                        <th>Adress</th>
                        <td>{requestDetails[0] && requestDetails[0].adress}</td>
                        <th>Serial Number</th>
                        <td>{requestDetails[0] && requestDetails[0].numeroSerie}</td>
                    </tr>
                    <tr>
                        <th>Device Reference</th>
                        <td>{requestDetails[0] && requestDetails[0].referenceDevice}</td>
                        <th>Model</th>
                        <td>{requestDetails[0] && requestDetails[0].disinfectedEquipment}</td>
                    </tr>
                    <tr>
                        <th>intervention sheet</th>
                        <td>
                            {requestDetails[0] && requestDetails[0].fileName1 ? (
                                <a href={`http://localhost:8080/api/request/download1/${reference}`} download>
                                    <i className="fas fa-file-pdf text-success"></i> 
                                </a>
                            ) : (
                                <span>No file</span>
                            )}
                        </td>
                        <th>diagnostic sheet</th>
                        <td>
                            {requestDetails[0] && requestDetails[0].fileName ? (
                                <a href={`http://localhost:8080/api/request/download/${reference}`} download>
                                    <i className="fas fa-file-pdf text-success"></i> 
                                </a>
                            ) : (
                                <span>No file</span>
                            )}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div className="d-flex justify-content-end">
                <button className="btn btn-danger me-3" onClick={cancelRequest}>Cancel</button>
            </div>
        </div>
    );
}

export default DetailsIntervention;
