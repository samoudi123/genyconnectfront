import React from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { useNavigate, useParams } from 'react-router-dom';
import { ThemeProvider } from '@emotion/react';
import { Button } from 'react-bootstrap';

function DetailsFiche (){
   



const navigate = useNavigate();
    const { reference } = useParams(); // Récupérer la référence de la demande à partir des paramètres d'URL
    const [requestDetails, setRequestDetails] = useState({});
        useEffect(() => {
            const getRequestDetails = async () => {
                try {
                    const response = await axios.get(`http://localhost:8080/api/request/reference/${reference}`);
                    setRequestDetails(response.data);
                    console.log(requestDetails);
                } catch (error) {
                    console.error("Error fetching request details:", error);
                }
            };
    
            getRequestDetails();
        }, [reference]);
    
        const cancelRequest = async () => {
            try {
                await axios.post(`http://localhost:8080/api/request/cancelAssignment/${reference}`);
                navigate(-1);
            } catch (error) {
                console.error("Error cancelling request:", error);
            }
        };
    
        const handleDownloadClick1 = async () => {
            try {
                const response = await axios.get(`http://localhost:8080/api/request/download1/${reference}`, {
                    responseType: 'blob',
                });
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', 'file.pdf');
                document.body.appendChild(link);
                link.click();
            } catch (error) {
                console.error("Error downloading file:", error);
            }
        };
    
        const handleDownloadClick = async () => {
            try {
                const response = await axios.get(`http://localhost:8080/api/request/download/${reference}`, {
                    responseType: 'blob',
                });
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', 'file.pdf');
                document.body.appendChild(link);
                link.click();
            } catch (error) {
                console.error("Error downloading file:", error);
            }
        };
       const file = requestDetails[0] && requestDetails[0].fileName
    
        return (
            <div className="container mt-4">
                <h1 className="mb-4">Maintenance Request Details</h1>
                <table className="table">
                    <tbody>
                        <tr>
                            <th>Device Type</th>
                            <td>{requestDetails[0] && requestDetails[0].typeAppareil}</td>
                            <th>Brand</th>
                            <td>{requestDetails[0] && requestDetails[0].marque}</td>
                        </tr>
                        <tr>
                            <th>Adress</th>
                            <td>{requestDetails[0] && requestDetails[0].adress}</td>
                            <th>Serial Number</th>
                            <td>{requestDetails[0] && requestDetails[0].numeroSerie}</td>
                        </tr>
                        <tr>
                            <th>Device Reference</th>
                            <td>{requestDetails[0] && requestDetails[0].referenceDevice}</td>
                            <th>Model</th>
                            <td>{requestDetails[0] && requestDetails[0].disinfectedEquipment}</td>
                        </tr>
                        <tr>
                            <th>intervention sheet</th>
                            <td>
                                {requestDetails[0] && requestDetails[0].fileName1 ? (
                                    <a href={`http://localhost:8080/api/request/download1/${reference}`} download>
                                        <i className="fas fa-file-pdf text-success"></i> 
                                    </a>
                                ) : (
                                    <span>No file</span>
                                )}
                            </td>
                            <th>diagnostic sheet</th>
                            <td>
                                {requestDetails[0] && requestDetails[0].fileName ? (
                                    <a href={`http://localhost:8080/api/request/download/${reference}`} download>
                                        <i className="fas fa-file-pdf text-success"></i> 
                                    </a>
                                ) : (
                                    <span>No file</span>
                                )}
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div className="d-flex justify-content-end">
                    <button className="btn btn-success me-3" onClick={cancelRequest}>Cancel</button>
                </div>
                <div className=" justify-content-start">
                    <Link to={`/diagnostic/${reference}`}>
                    <button className="btn btn-success me-3"disabled={!!file } >Accept</button>
                    </Link>
                </div>
            </div>
        );
    }
    
    export default DetailsFiche;
    
