import React, { useState } from 'react';
import logoLeft from '../Assets/Fichier 7 (1).png'; // Importez votre logo à gauche
import 'react-phone-number-input/style.css'
import axios from  "axios";
import photo from '../Assets/1.png';

import { useLocation, useNavigate } from 'react-router-dom';
import PhoneInput from 'react-phone-number-input'
import {
  MDBBtn,
  MDBContainer,
  MDBCard,
  MDBCardBody,
  MDBInput,
  MDBIcon,
  MDBRow,
  MDBCol,
  MDBCheckbox
}
from 'mdb-react-ui-kit';

function SignupTechnician() {

  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);
  const role = queryParams.get('role');
  const [firstname, setFirstname] = useState('');
  const [lastname, setLastname] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [adress, setAdress] = useState('');
  const [businessName, setBusinessName] = useState('');
  const [diplome, setDiplome] = useState('');
  const [specialityy, setSpecialtyy] = useState('');
  const [nbreIntervention, setNbreIntervention] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [experience, setExperience] = useState('');
  const [errors, setErrors] = useState({});
  const navigate = useNavigate();

  const saveTechnician = async (event) => {
    event.preventDefault();
    const newErrors = {};

    // Vérification des champs requis
    if (!firstname) newErrors.firstname = 'Please enter your first name';
    if (!lastname) newErrors.lastname = 'Please enter your last name';
    if (!email) newErrors.email = 'Please enter your email';
    if (!password) newErrors.password = 'Please enter your password';
    if (!adress) newErrors.adress = 'Please enter your address';
    if (!businessName) newErrors.businessName = 'Please enter your business name';
    if (!diplome) newErrors.diplome = 'Please enter your diplome';
    if (!specialityy) newErrors.specialityy = 'Please enter your speciality';
    if (!nbreIntervention) newErrors.nbreIntervention = 'Please enter the number of interventions';
    if (!experience) newErrors.experience = 'Please enter your experience';
    if (!phoneNumber) newErrors.phoneNumber = 'Please enter your phone number';

    // Vérification du format de l'email
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (email && !emailRegex.test(email)) {
      newErrors.email = 'Please enter a valid email address';
    }

    // Vérification du mot de passe
    const passwordRegex = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/;
    if (password && !passwordRegex.test(password)) {
      newErrors.password = 'Password must contain at least 8 characters, one letter, one number, and one special character';
    }

    if (Object.keys(newErrors).length > 0) {
      setErrors(newErrors);
      return;
    }

    try {
      await axios.post('http://localhost:8080/api/v1/auth/register', {
        firstname: firstname,
        lastname: lastname,
        email: email,
        password: password,
        adress: adress,
        businessName: businessName,
        diplome: diplome,
        specialityy: specialityy,
        nbreIntervention: nbreIntervention,
        experience: experience,
        phoneNumber: phoneNumber,
        role: role || 'TECHNICIAN'
      });
      navigate("/login");

    } catch (error) {
      setErrors({ apiError: 'Registration failed. Please try again.' });
    }
  };

  const cssStyles = `
  .cascading-right {
    margin-right: -40px;
  }
  
  @media (max-width: 900px) {
    .cascading-right {
      margin-right: 1;
    }
  }
  .logo {
    position: absolute;
    top: -90px;
    left: 20px;
    width: 200px; 
    height: auto;
    z-index: 1;
  }
  `;


  return (
    <>
          <style>{cssStyles}</style>

  <div style={{ position: 'absolute', top: '170px', left: '300px' }}>
  <img className="logo" src={logoLeft} alt="Logo" />
</div>      <MDBContainer fluid className='my-2'>
        <MDBRow className='g-3 align-items-center justify-content-center' style={{ minHeight: '100vh' }}>
          <MDBCol md='5'> {/* Utilisation de col-md-6 pour contrôler la taille du conteneur */}
            <MDBCard className='my-5 cascading-right' style={{ background: 'hsla(0, 0%, 100%, 0.55)', backdropFilter: 'blur(30px)' }}>
              <MDBCardBody className='p-5 shadow-5 text-center'>
                <h2 className="fw-bold mb-5"></h2>
                <MDBRow>
                  <MDBCol col='6'>
                    <MDBInput wrapperClass='mb-4' label='FirstName' id='form1' type='text' value={firstname} onChange={(e) => setFirstname(e.target.value)}  />
                  </MDBCol>
                  <MDBCol col='6'>
                    <MDBInput wrapperClass='mb-4' label='LastName' id='form2' type='text' value={lastname} onChange={(e) => setLastname(e.target.value)}  />
                  </MDBCol>
                </MDBRow>
                <MDBInput wrapperClass='mb-4' label='Email' id='form3' type='email'  value={email} onChange={(e) => setEmail(e.target.value)} />
                <MDBInput wrapperClass='mb-4' label='Password' id='form4' type='password' value={password} onChange={(e) => setPassword(e.target.value)}  />
                <MDBInput wrapperClass='mb-4' label='Adress' id='form5' type='text' value={adress} onChange={(e) => setAdress(e.target.value)}  />
                <MDBInput wrapperClass='mb-4' label='Business Name' id='form6' type='text' value={businessName} onChange={(e) => setBusinessName(e.target.value)}  />
                <MDBInput wrapperClass='mb-4' label='Diploma' id='form6' type='text' value={diplome} onChange={(e) => setDiplome(e.target.value)}  />
                <MDBInput wrapperClass='mb-4' label='Speciality' id='form6' type='text' value={specialityy} onChange={(e) => setSpecialtyy(e.target.value)}  />


                <MDBRow>
                  <MDBCol col='6'>
                    <MDBInput wrapperClass='mb-4' label='Number of intervention' id='form1' type='text' value={nbreIntervention} onChange={(e) => setNbreIntervention(e.target.value)}  />
                  </MDBCol>
                  <MDBCol col='6'>
                    <MDBInput wrapperClass='mb-4' label='Experience' id='form2' type='text' value={experience} onChange={(e) => setExperience(e.target.value)}   />  

                  </MDBCol>
                  <PhoneInput
      placeholder="Enter phone number"
      value={phoneNumber}
      onChange={setPhoneNumber}/>

                </MDBRow>                <div className='d-flex justify-content-center mb-4'>
                </div>
                <MDBBtn className='w-100 mb-4' size='md' onClick={saveTechnician}>Sign up</MDBBtn>
                <div className="text-center"></div>
              </MDBCardBody>
            </MDBCard>
          </MDBCol>
          <MDBCol md='6'>
            <img src={photo} className="w-100 round ed-4 shadow-4" alt="" fluid />
          </MDBCol>
        </MDBRow>
      </MDBContainer>
    </>
  );
}

export default SignupTechnician;
