import React, { useState, useEffect, useRef } from "react";
import { ZegoUIKitPrebuilt } from '@zegocloud/zego-uikit-prebuilt';
import { useParams } from "react-router-dom";
import axios from "axios";

function Room() {
    const [currentUserDetails, setCurrentUserDetails] = useState(null); // Use null to indicate no data
    const [isLoading, setIsLoading] = useState(true); // Loading state
    const containerRef = useRef(null); // Ref for the meeting container

    const { roomID } = useParams(); // Get roomID from the URL

    useEffect(() => {
        const fetchCurrentUserDetails = async () => {
            try {
                const accessToken = localStorage.getItem('accessToken');
                const userDetailsResponse = await axios.get('http://localhost:8080/api/v1/users/currentUser', {
                    headers: {
                        Authorization: `Bearer ${accessToken}`,
                    },
                });

                const currentUserDetails = userDetailsResponse.data;
                setCurrentUserDetails(currentUserDetails);
                setIsLoading(false); // Data fetched, stop loading
            } catch (error) {
                console.error('Failed to fetch user details', error);
                setIsLoading(false); // Stop loading on error too
            }
        };

        fetchCurrentUserDetails();
    }, []);

    useEffect(() => {
        if (currentUserDetails && containerRef.current) {
            const username = currentUserDetails.firstname;
            const appID = 402128907;
            const serverSecret = "75eca6284575787cd5492d20aa0e88d2";
            const kitToken = ZegoUIKitPrebuilt.generateKitTokenForTest(appID, serverSecret, roomID, Date.now().toString(), username);
            const zp = ZegoUIKitPrebuilt.create(kitToken);
            // start the call
            zp.joinRoom({
                container: containerRef.current,
                sharedLinks: [
                    {
                        name: 'copy link',
                        url: `http://localhost:3000/room/${roomID}`
                    },
                ],
                scenario: {
                    mode: ZegoUIKitPrebuilt.OneONoneCall,
                },
            });
        }
    }, [currentUserDetails, roomID]);

    if (isLoading) {
        return <div>Loading...</div>; // Display loading state
    }

    if (!currentUserDetails) {
        return <div>Failed to load user details</div>; // Handle the case where user details are not available
    }

    return (
        <div ref={containerRef}></div>
    );
}

export default Room;
