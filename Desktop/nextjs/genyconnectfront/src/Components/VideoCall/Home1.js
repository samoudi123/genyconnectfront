import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

function Home1(){
    const [roomID,setRoomID]=useState();
    const navigate = useNavigate();
    const handleJoin = ()=>{
        navigate(`/room/${roomID}`);
    }
    return( 
        <div className="App">
            <input placeholder="Enter Room Id" type="text"
            value={roomID} onChange={(e)=>setRoomID(e.target.value)}/>
            <button onClick={handleJoin}>join</button>

        </div>
    );
}
export default Home1;