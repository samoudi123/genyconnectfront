import React from "react";
import Stripe from "react-stripe-checkout";
import { useState } from "react";
import { useEffect } from "react";
import axios from "axios";
function Payment() {
  const [amount, setAmount] = useState(0);
  useEffect(() => {
    async function fetchAmount() {
      try {
        const response = await axios.get("http://localhost:8080/steps/calculateCostPerStep/");
        setAmount(response.data.amount); 
      } catch (error) {
        console.error("Error fetching amount:", error);
      }
    }

    fetchAmount();
  }, []);
async function handleToken(token) {
console.log(token);
await axios.post("http://localhost:8080/api/payment/charge", "", {         headers: {
  token: token.id,
  amount:amount,
},}).then(() => {
   alert("Payment Success");
   }).catch((error) => {
   alert(error);
   });
}
return (
<div className="App">
<Stripe
stripeKey="pk_test_51PJd1PRpfwCeXZh3zzmUjOgbQlbkcLO9I9l9WO0IDV0rgv7YUx9OSl0OsleP0ba0wyiJN5Zej3nLxCL2jhGO4WnU00I6OD45cn"
token={handleToken}
/>
</div>
);
}
export default Payment;