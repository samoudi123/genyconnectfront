import LinearStepper from "../progress/timeline";
import { CssBaseline, Container, Paper, Box } from "@material-ui/core";

function Step() {
  return (
    <>
      <CssBaseline />
      <Container component={Box} p={4}>
        <Paper component={Box} p={3}>
          <LinearStepper />
        </Paper>
      </Container>
    </>
  );
}

export default Step;