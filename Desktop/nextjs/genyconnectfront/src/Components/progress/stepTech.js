import { CssBaseline, Container, Paper, Box } from "@material-ui/core";
import LinearStepper1 from "./timelineTech";

function StepTech() {
  return (
    <>
      <CssBaseline />
      <Container component={Box} p={4}>
        <Paper component={Box} p={3}>
          <LinearStepper1 />
        </Paper>
      </Container>
    </>
  );
}

export default StepTech;