import React, { useState, useEffect } from "react";
import {
  Typography,
  Button,
  Stepper,
  Step,
  StepLabel,
  Box,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Axios from "axios";
import { useParams } from "react-router-dom";
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import PaymentIcon from '@material-ui/icons/Payment';
import StripeCheckout from "react-stripe-checkout";
import axios from "axios";
const useStyles = makeStyles((theme) => ({
  button: {
    marginRight: theme.spacing(1),
  },
  input: {
    display: 'none',
  },
  fileLabel: {
    marginTop: theme.spacing(2),
  },
  fileInputContainer: {
    marginTop: theme.spacing(4),
  },
  icon: {
    verticalAlign: 'middle',
    marginLeft: theme.spacing(1),
    color: '#2196F3',
  },
  mainContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100vh',
    flexDirection: 'row', // S'assurer que les éléments sont disposés horizontalement
  },
  stepperContainer: {
    width: '60%',
    padding: theme.spacing(5),
    border: '4px solid #B3DEE8',
    borderRadius: theme.spacing(2),
    marginLeft: theme.spacing(2), 
  },
  detailsContainer: {
    padding: theme.spacing(2),
    border: '4px solid #B3DEE8',
    borderRadius: theme.spacing(1),
    backgroundColor: '#f9f9f9',
    width: '300px', 
    height: '300px', 
    marginRight: theme.spacing(2), },
  paymentMessage: {
    color: 'red',
    marginTop: theme.spacing(2),
  },
  paymentButton: {
    marginTop: theme.spacing(2),
    backgroundColor: '#2196F3',
    color: '#FFF',
  },
}));

const LinearStepper = () => {
  const { reference } = useParams();
  const [currentStepId, setCurrentStepId] = useState(null);

  const classes = useStyles();
  const [activeStep, setActiveStep] = useState(0);
  const [steps, setSteps] = useState([]);
  const [files, setFiles] = useState({});
  const [paymentAmount, setPaymentAmount] = useState(null);
  const [paidSteps, setPaidSteps] = useState([]);

  useEffect(() => {
    Axios.get(`http://localhost:8080/api/request/steps/${reference}`)
      .then((response) => {
        const numSteps = response.data;
        setSteps(Array.from({ length: numSteps }, (_, index) => ({
          label: `Step ${index + 1}`, id: null, proofAdded: false, requiresPayment: false 
        })));
        console.log("Initial steps:", numSteps);
      })
      .catch((error) => {
        console.error("Error fetching number of steps:", error);
      });
  }, [reference]);

  useEffect(() => {
    Axios.get(`http://localhost:8080/steps/calculateCostPerStep/${reference}`)
      .then((response) => {
        console.log(response.data);
        setPaymentAmount(response.data); // Assurez-vous que la réponse contient le montant
        console.log(paymentAmount);
      })
      .catch((error) => {
        console.error("Error fetching payment amount:", error);
      });
  }, [reference]);
  const allStepsCompleted = () => {
    return steps.every(step => step.proofAdded || paidSteps.includes(step.id));
  };

  useEffect(() => {
    Axios.get(`http://localhost:8080/steps/list/${reference}`)
      .then((response) => {
        const stepsData = response.data;
        console.log("Fetched steps data:", stepsData);
        setSteps(prevSteps => {
          const updatedSteps = prevSteps.map((step, index) => ({
            ...step,
            id: stepsData[index]?.id,
            proofAdded: stepsData[index]?.proofAdded,
            requiresPayment: stepsData[index]?.requiresPayment,
            description: stepsData[index]?.description, // Ajoutez la description ici
          }));
          console.log("Updated steps state:", updatedSteps);
          return updatedSteps;
        });
      })
      .catch((error) => {
        console.error("Error fetching steps list:", error);
      });
  }, [reference]);

  useEffect(() => {
    Axios.get(`http://localhost:8080/steps/status/termine/${reference}`)
      .then((response) => {
        const currentStepId = response.data;
        console.log("Current step ID with status 'en cours':", currentStepId);
        setCurrentStepId(currentStepId);
      })
      .catch((error) => {
        console.error("Error fetching current step ID:", error);
      });
  }, [reference]);

  useEffect(() => {
    if (steps[activeStep]?.requiresPayment) {
      markStepAsCompleted(activeStep);
      handleNext();
    }
  }, [steps, activeStep]);

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const markStepAsCompleted = (stepIndex) => {
    setSteps(prevSteps =>
      prevSteps.map((step, index) =>
        index === stepIndex ? { ...step, requiresPayment: true, status: 'completed' } : step
      )
    );
  };

  const handleToken = async (token) => {
    try {
      await Axios.post(`http://localhost:8080/api/payment/charge/${currentStepId}`, {}, {
        headers: {
          token: token.id,
          amount: paymentAmount,
        },
      });
      alert("Payment Success");
      setPaidSteps([...paidSteps, activeStep]);
      handleNext();
    } catch (error) {
      alert(error);
    }
  };
  console.log(steps[activeStep]);

  const handleDownloadClick1 = async () => {
    try {
        const response = await axios.get(`http://localhost:8080/steps/download2/${currentStepId}`, {
            responseType: 'blob',
        });
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', 'file.pdf');
        document.body.appendChild(link);
        link.click();
    } catch (error) {
        console.error("Error downloading file:", error);
    }
};
  return (
    <Box className={classes.mainContainer}>
      {steps[activeStep] && (
        <Box className={classes.detailsContainer}>
          <Typography variant="h6">Details for Step {activeStep + 1}:</Typography>
          <Typography variant="body1">{steps[activeStep]?.description}</Typography>
          <Typography variant="body1">Amount: {paymentAmount}$</Typography>
        </Box>
      )}

      <div className={classes.stepperContainer}>
        <Stepper alternativeLabel activeStep={activeStep}>
          {steps.map((step, index) => (
            <Step key={step.label}>
              <StepLabel>
                {step.label}
                {(step.proofAdded || paidSteps.includes(index)) && (
                  <CheckCircleIcon className={classes.icon} color="primary" />
                )}
                {step.requiresPayment && (
                  <PaymentIcon className={classes.icon} color="secondary" />
                )}
              </StepLabel>
            </Step>
          ))}
        </Stepper>

        {activeStep === steps.length ? (
          <>
            <Typography variant="h3" align="center">
              Thank You
            </Typography>
            {allStepsCompleted() && (
              <Button
                variant="contained"
                color="primary"
                className={classes.button}
              >
                Complete
              </Button>
            )}
          </>
        ) : (
          <div>
            <Typography variant="h6">Step {activeStep + 1}:</Typography>

            {!steps[activeStep]?.requiresPayment &&steps[activeStep]?.proofAdded && (
              <><StripeCheckout
                  stripeKey="pk_test_51PJd1PRpfwCeXZh3zzmUjOgbQlbkcLO9I9l9WO0IDV0rgv7YUx9OSl0OsleP0ba0wyiJN5Zej3nLxCL2jhGO4WnU00I6OD45cn"
                  token={handleToken}
                  amount={paymentAmount * 100}
                  name={`Payment for Step ${activeStep + 1}`}
                >
                  <Button
                    variant="contained"
                    color="secondary"
                    className={classes.paymentButton}
                  >
                    Pay
                  </Button>
                  <Typography variant="body1" color="textSecondary">
                    (you have to pay for this step)
                  </Typography>
                </StripeCheckout><Box display="flex" alignItems="center">
                    <CheckCircleIcon
                      className={classes.icon}
                      onClick={() => handleDownloadClick1(steps[activeStep]?.id)} />
                    <Typography variant="body2">Check technician’s proof
                    </Typography>

                  </Box></>
              
            )}
          </div>
        )}
      </div>
    
    </Box>
  );
};
export default LinearStepper;
