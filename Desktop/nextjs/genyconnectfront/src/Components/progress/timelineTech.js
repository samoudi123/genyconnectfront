import React, { useState, useEffect } from "react";
import CancelIcon from '@material-ui/icons/Cancel';

import {
  Typography,
  Button,
  Stepper,
  Step,
  StepLabel,
  Box,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Axios from "axios";
import { useParams } from "react-router-dom";
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import PaymentIcon from '@material-ui/icons/Payment';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';

const useStyles = makeStyles((theme) => ({
  button: {
    marginRight: theme.spacing(1),
  },
  input: {
    display: 'none',
  },
  fileLabel: {
    marginTop: theme.spacing(2),
    display: 'flex',
    alignItems: 'center',
  },
  fileInputContainer: {
    marginTop: theme.spacing(4),
    
  },
  icon: {
    verticalAlign: 'middle',
    marginLeft: theme.spacing(1),
  },
  iconContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  centeredContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100vh',
    flexDirection: 'row', // S'assurer que les éléments sont disposés horizontalement

  },
  detailsContainer: {
    padding: theme.spacing(2),
    border: '4px solid #B3DEE8',
    borderRadius: theme.spacing(1),
    backgroundColor: '#f9f9f9',
    width: '300px', 
    height: '350px', 
    marginRight: theme.spacing(2), },
  stepperContainer: {
    width: '60%',
    padding: theme.spacing(5),
    border: '4px solid #B3DEE8',
    borderRadius: theme.spacing(2),
    marginLeft: theme.spacing(2),
  },
  uploadButton: {
    marginTop: theme.spacing(1),

  },
 
  fileName: {
    marginTop: theme.spacing(1),
    fontWeight: 'bold',
  },
}));

const LinearStepper1 = () => {
  const { reference } = useParams();
  const classes = useStyles();
  const [activeStep, setActiveStep] = useState(0);
  const [steps, setSteps] = useState([]);
  const [files, setFiles] = useState({});
  const [fileNames, setFileNames] = useState({});
  const [stepValid, setStepValid] = useState({});
  const [activeStepDetails, setActiveStepDetails] = useState(null);
  const [allStepsCompleted, setAllStepsCompleted] = useState(false);
  const [paymentAmount, setPaymentAmount] = useState(null);


  useEffect(() => {
    Axios.get(`http://localhost:8080/api/request/steps/${reference}`)
      .then((response) => {
        const numSteps = response.data;
        setSteps(Array.from({ length: numSteps }, (_, index) => ({ label: `Step ${index + 1}`, id: null })));
      })
      .catch((error) => {
        console.error("Error fetching number of steps:", error);
      });
  }, [reference]);

  useEffect(() => {
    Axios.get(`http://localhost:8080/steps/list/${reference}`)
      .then((response) => {
        const stepsData = response.data;
        setSteps(prevSteps => prevSteps.map((step, index) => ({
          ...step,
          id: stepsData[index]?.id,
          proofAdded: stepsData[index]?.proofAdded,
          status: stepsData[index]?.status,
          requiresPayment: stepsData[index]?.requiresPayment,
          description: stepsData[index]?.description, // Ajoutez la description ici

        })));
      })
      .catch((error) => {
        console.error("Error fetching steps list:", error);
      });
  }, [reference]);

  useEffect(() => {
    if (steps[activeStep]?.requiresPayment) {
      markStepAsCompleted(activeStep);
      handleNext();
    }
  }, [steps, activeStep]);

  useEffect(() => {
    const allStepsCompleted = steps.every(step => step.status === 'completed');
    setAllStepsCompleted(allStepsCompleted);
    if (allStepsCompleted) {
      setActiveStep(steps.length);
    }
  }, [steps]);

  const handleNext = () => {
    if (activeStep < steps.length - 1) {
      setActiveStep((prevActiveStep) => prevActiveStep + 1);
      setActiveStepDetails(steps[activeStep + 1]);
    }
  };

  const markStepAsCompleted = (stepIndex) => {
    setSteps(prevSteps =>
      prevSteps.map((step, index) =>
        index === stepIndex ? { ...step, requiresPayment: true, status: 'completed' } : step
      )
    );
  };

  const handleFileChange = (event, stepIndex) => {
    const file = event.target.files[0];
    if (file) {
      setFiles({ ...files, [stepIndex]: file });
      setFileNames({ ...fileNames, [stepIndex]: file.name });

      const step = steps[stepIndex];
      if (step && step.id) {
        const fileType = file.type;
        if (fileType === "application/pdf" || fileType.startsWith("image/")) {
          const formData = new FormData();
          formData.append("file", file);
          Axios.post(`http://localhost:8080/steps/proof/${step.id}`, formData)
            .then(response => {
              setStepValid({ ...stepValid, [stepIndex]: true });
              markStepAsCompleted(stepIndex);
            })
            .catch(error => {
              console.error("Error uploading file:", error);
              setStepValid({ ...stepValid, [stepIndex]: false });
            });
        } else {
          setStepValid({ ...stepValid, [stepIndex]: false });
        }
      } else {
        console.error("Step ID not found");
      }
    }
  };
  useEffect(() => {
    Axios.get(`http://localhost:8080/steps/calculateCostPerStep/${reference}`)
      .then((response) => {
        console.log(response.data);
        setPaymentAmount(response.data); // Assurez-vous que la réponse contient le montant
        console.log(paymentAmount);
      })
      .catch((error) => {
        console.error("Error fetching payment amount:", error);
      });
  }, [reference]);
  console.log(paymentAmount);
  return (
    <Box className={classes.centeredContainer}>
      {steps[activeStep] && (
        <Box className={classes.detailsContainer}>
          <Typography variant="h6">Details for Step {activeStep + 1}:</Typography>
          <Typography variant="body1">{steps[activeStep]?.description}</Typography>
          <Typography variant="body1">Amount: {paymentAmount}$</Typography>

      
        </Box>
      )}
    
      <div className={classes.stepperContainer}>
        <Stepper alternativeLabel activeStep={activeStep}>
          {steps.map((step, index) => (
            <Step key={step.label}>
              <StepLabel>
                <div className={classes.iconContainer}>
                  {step.label}
                  {step.proofAdded && (
                    <CheckCircleIcon className={classes.icon} color="primary" />
                  )}
                  {step.requiresPayment && (
                    <PaymentIcon className={classes.icon} color="secondary" />
                  )}
                </div>
              </StepLabel>
            </Step>
          ))}
        </Stepper>

        {activeStep === steps.length ? (
          <>
            <Typography variant="h3" align="center">
              Thank You
            </Typography>
            {allStepsCompleted && (
              <Button
                variant="contained"
                color="primary"
                className={classes.button}
              >
                Complete
              </Button>
            )}
          </>
        ) : (
          <div>
            <Typography variant="h6">Step {activeStep + 1}:</Typography>
            { !steps[activeStep]?.proofAdded && (

            <Box className={classes.fileInputContainer}>
              <input
                accept="application/pdf,image/*"
                className={classes.input}
                id={`file-input-${activeStep}`}
                type="file"
                onChange={(event) => handleFileChange(event, activeStep)} />
              <label htmlFor={`file-input-${activeStep}`}>
                <Button
                  variant="contained"
                  color="primary"
                  component="span"
                  className={classes.uploadButton}
                  startIcon={<CloudUploadIcon />}
                >
                  Upload File
                </Button>

              

              </label>
              {fileNames[activeStep] && (
                <Typography variant="body1" className={classes.fileName}>
                  {fileNames[activeStep]}
                </Typography>
              )}
               <Typography variant="body1" color="textSecondary">
      (You must provide your proof to validate the step)
    </Typography>
            </Box>
                        )}
        
          </div>
        )}
      </div>
    </Box>
  );
};

export default LinearStepper1;
