import React from 'react';
import '../profile/SideBar.css';
import { useEffect } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom'; // Importez Link depuis react-router-dom

import { useState } from 'react';
const SideBar2 = () => {
    const [currentUserDetails, setCurrentUserDetails] = useState(null);

   

    useEffect(() => {
        const fetchCurrentUserDetails = async () => {
            try {
                const accessToken = localStorage.getItem('accessToken');
                const userDetailsResponse = await axios.get('http://localhost:8080/api/v1/users/currentUser', {
                    headers: {
                        Authorization: `Bearer ${accessToken}`,
                    },
                });

                const currentUserDetails = userDetailsResponse.data;
                setCurrentUserDetails(currentUserDetails);
                console.log(currentUserDetails);

            } catch (error) {
                console.error('Failed to fetch user details', error);
            }
        };

        fetchCurrentUserDetails();
    }, []);

    const [isProfileHovered, setIsProfileHovered] = useState(false);

    const handleProfileHover = () => {
        setIsProfileHovered(true);
    };

    const handleProfileLeave = () => {
        setIsProfileHovered(false);
    };
    return (
        <div className="wrapper">
            <aside id="sidebar">
                <div className="h-100">
                    <div className="sidebar-logo">
                        <a >GenyConnect</a>
                    </div>
                    <ul className="sidebar-nav">
                        <li className="sidebar-item">
                            <Link to={`/ProfileTechnician/${currentUserDetails?.id}`} className='sidebar-link '
                               >
                                <i className="fa-solid fa-user pe-2"></i>
                                 Profile
                            </Link>
                        </li>
                        <li className="sidebar-item">
                        <a href="#" className="sidebar-link ">

                            <i class="fa-solid fa-user-pen pe-2"></i>
                                 Edit Profile
                                 </a>
                        </li>
                        <li className="sidebar-item">
                        <Link to={`/interventionTech/${currentUserDetails?.id}`} className='sidebar-link' >
                                <i className="fa-solid fa-list pe-2"></i>
                                 Intervention
                                 </Link>
                        </li>
                        <li className="sidebar-item">
                        <Link to={`/form1/${currentUserDetails?.id}`} className='sidebar-link' >
                            <i class="fa-solid fa-comment pe-2"></i>                  
                                          Feedback
                            </Link>
                        </li>
                        <li className="sidebar-item">
                            <a href="#" className="sidebar-link ">
                            <i class="fa-sharp fa-solid fa-circle-info pe-2"></i>                          
                                  Help
                            </a>
                           
                        </li>
                        <li className="sidebar-item">
                            <a href="#" className="sidebar-link ">
                            <i class="fa-sharp fa-solid fa-gear pe-2"></i>              
                                          Logout                       </a>
                           
                        </li>
                        {/* Ajoutez les autres éléments de la barre latérale ici */}
                    </ul>
                </div>
            </aside>
        </div>
    );
};

export default SideBar2;
