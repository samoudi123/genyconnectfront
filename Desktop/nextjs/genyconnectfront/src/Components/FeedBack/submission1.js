import React, { useState, useEffect } from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Table from 'react-bootstrap/Table';
import axios from 'axios'; 
import { useParams, Link } from 'react-router-dom';
import SideBar1 from '../profile/SideBar';

function Submissions1() {
    const cssStyles = `
    .emp-profile {
        padding: 3%;
        margin-top: 3%;
        margin-bottom: 3%;
        border-radius: 0.5rem;
        background: #fff;
    }
    .profile-img {
        text-align: center;
    }
    .profile-img img {
        width: 70%;
        height: 100%;
    }
    .profile-img .file {
        position: relative;
        overflow: hidden;
        margin-top: -20%;
        width: 70%;
        border: none;
        border-radius: 0;
        font-size: 15px;
        background: #212529b8;
    }
    .profile-img .file input {
        position: absolute;
        opacity: 0;
        right: 0;
        top: 0;
    }
    .profile-head h5 {
        color: #333;
    }
    .profile-head h6 {
        color: #0062cc;
    }
    .profile-edit-btn {
        border: none;
        border-radius: 1.5rem;
        width: 70%;
        padding: 2%;
        font-weight: 600;
        color: #6c757d;
        cursor: pointer;
    }
    .proile-rating {
        font-size: 12px;
        color: #818182;
        margin-top: 5%;
    }
    .proile-rating span {
        color: #495057;
        font-size: 15px;
        font-weight: 600;
    }
    .profile-head .nav-tabs {
        margin-bottom: 5%;
    }
    .profile-head .nav-tabs .nav-link {
        font-weight: 600;
        border: none;
    }
    .profile-head .nav-tabs .nav-link.active {
        border: none;
        border-bottom: 2px solid #0062cc;
    }
    .profile-work {
        padding: 14%;
        margin-top: -15%;
    }
    .profile-work p {
        font-size: 12px;
        color: #818182;
        font-weight: 600;
        margin-top: 10%;
    }
    .profile-work a {
        text-decoration: none;
        color: #495057;
        font-weight: 600;
        font-size: 14px;
    }
    .profile-work ul {
        list-style: none;
    }
    .profile-tab label {
        font-weight: 600;
    }
    .profile-tab p {
        font-weight: 600;
        color: #0062cc;
    }
    body, html {
        margin: 10;
        padding: 10;
    }
    .app-container {
        width: 100%;
        max-width: none;
        margin: 0;
        padding: 0;
    }
    .navbar {
        width: 93%;
        position: fixed;
        top: 0;
        z-index: 1000;
    }
`;
    const { userId } = useParams();   
    const [feedbackList, setFeedbackList] = useState([]); 
    const [loading, setLoading] = useState(true); 

    useEffect(() => {
        axios.get(`http://localhost:8080/api/list1/${userId}`)
            .then(response => {
                if (Array.isArray(response.data)) {
                    setFeedbackList(response.data); 
                    setLoading(false); 
                } else {
                    console.error('Invalid data format:', response.data);
                    setLoading(false); 
                }
            })
            .catch(error => {
                console.error('Error fetching feedback data:', error);
                setLoading(false); 
            });
    }, [userId]);
 
    const handleCheckVal = (entry, question) => {
        if (entry && entry[question]) {
            return entry[question];
        }
        return '';
    };
    
        
    const feedbackQuestions = {
        'question1': 'How would you rate the user interface friendliness of the platform for searching medical equipment repair missions?',
        'question2': 'To what extent did the platform facilitate managing logistical details of repair interventions, such as location and scheduling?',
        'question3': 'How satisfied are you with the variety of repair missions available on the platform, in terms of types of medical equipment and complexity levels?',
        'question4': 'Regarding customer support and issue resolution during interventions, how would you assess the platform effectiveness?'
    };
    
    return (
        <>
        <style>
                {`
                .feedback-container {
                    padding: 10px;
                    margin-left: 220px; 
                }
                `}
            </style>
        <style>{cssStyles}</style>
            <nav className="navbar navbar-expand-xl navbar-blue bg-blue">
                <div className="container-fluid">
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav me-auto mb-2 mb-lg-0"></ul>
                        <form className="d-flex justify-content-end">
                            <Link to="/chat">Conversations</Link>
                            <span style={{ margin: '0 10px' }}></span>
                            
                        </form>
                    </div>
                </div>
            </nav>
        
            <div className="row">
                <div className="col-md-2">
                    <SideBar1 userId={userId} />
                </div>
                <div className='feedback-container'>
                
                <div className="col-md-10">
                    {loading ? (
                        <div>Loading...</div>
                    ) : (
                        <div className='padding30px'>
                            <Table striped hover responsive>
                                <thead>
                                    <tr>
                                        <th> Name</th>
                                        <th>Reference</th>
                                        <th>Comments</th>
                                        {Object.keys(feedbackQuestions).map((question) => (
                                            <th key={question}>{feedbackQuestions[question]}</th>
                                        ))}
                                    </tr>
                                </thead>
                                <tbody>
                                    {feedbackList.map((entry, index) => (
                                        <tr key={index}>
                                           
                                            <td>{entry.clientName}</td>
                                            <td>{entry.reference}</td>
                                            <td>{entry.comments}</td>
                                            {Object.keys(feedbackQuestions).map((question) => (
                                                <td key={question}>{handleCheckVal(entry, question)}</td>
                                            ))}
                                        </tr>
                                    ))}
                                </tbody>
                            </Table>

                        </div>

                    )}
                                                </div>

                    
                </div>
            </div>

        </>
    );
}

export default Submissions1;
