import React, { useState } from 'react';
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import { InputGroup, Row, Col, Button, Alert } from 'react-bootstrap';
import 'react-phone-number-input/style.css';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router-dom';
import SideBar1 from '../profile/SideBar';
function FeedbackForm1() {
    const { userId } = useParams();

    const [displayForm, setDisplayForm] = useState(true);
    const [reference, setReference] = useState('');
    const [clientName, setClientName] = useState('');
    const [comments, setComments] = useState('');
    const [checkedValues, setCheckedValues] = useState([]);
    const [errors, setErrors] = useState({});

    const handleOnChange = (isChecked, value, question) => {
        let temp = [...checkedValues];
        if (isChecked) {
            temp = temp.filter(item => item.question !== question);
            temp.push({ question, value });
            setCheckedValues(temp);
        } else {
            setCheckedValues(temp.filter(item => item.value !== value));
        }
    };

    const formData = {
        clientName: clientName,
        reference: reference,
        Comments: comments,
        question1: checkedValues.find(item => item.question === 'qos')?.value || '',
        question2: checkedValues.find(item => item.question === 'qob')?.value || '',
        question3: checkedValues.find(item => item.question === 'roc')?.value || '',
        question4: checkedValues.find(item => item.question === 'exp')?.value || ''
    };

    const addFeedback = async () => {
        try {
            const response = await fetch(`http://localhost:8080/api/add1/${userId}`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(formData)
            });

            if (!response.ok) {
                throw new Error('Failed to add feedback');
            }

            // Si la requête est réussie, masquez le formulaire et affichez un message de confirmation
            setDisplayForm(false);
        } catch (error) {
            console.error('Error adding feedback:', error);
            // Gérer l'erreur, par exemple afficher un message d'erreur à l'utilisateur
        }
    };

    const validateForm = () => {
        let errors = {};
        if (clientName === '') errors.clientName = 'Please enter the value for the above field';
        if (reference === '') errors.reference = 'Please enter the value for the above field';
        if (comments === '') errors.comments = 'Please enter the value for the above field';

        Object.keys(feedbackType).forEach((key) => {
            if (!checkedValues.some(item => item.question === key)) {
                errors[key] = 'Please enter the value for the above field';
            }
        });

        setErrors(errors);
        return Object.keys(errors).length === 0;
    };

    const formSubmit = (e) => {
        e.preventDefault();

        if (validateForm()) {
            addFeedback();
        }
    };

    const feedbackType = {
        'qos': 'How would you rate the user interface friendliness of the platform for searching medical equipment repair missions?',
        'qob': 'To what extent did the platform facilitate managing logistical details of repair interventions, such as location and scheduling?',
        'roc': 'How satisfied are you with the variety of repair missions available on the platform, in terms of types of medical equipment and complexity levels?',
        'exp': 'Regarding customer support and issue resolution during interventions, how would you assess the platform effectiveness?'
    };

    const feedbackOpts = ['Excellent', 'Good', 'Fair', 'Bad'];
    const cssStyles = `
    .emp-profile {
        padding: 3%;
        margin-top: 3%;
        margin-bottom: 3%;
        border-radius: 0.5rem;
        background: #fff;
    }
    .profile-img {
        text-align: center;
    }
    .profile-img img {
        width: 70%;
        height: 100%;
    }
    .profile-img .file {
        position: relative;
        overflow: hidden;
        margin-top: -20%;
        width: 70%;
        border: none;
        border-radius: 0;
        font-size: 15px;
        background: #212529b8;
    }
    .profile-img .file input {
        position: absolute;
        opacity: 0;
        right: 0;
        top: 0;
    }
    .profile-head h5 {
        color: #333;
    }
    .profile-head h6 {
        color: #0062cc;
    }
    .profile-edit-btn {
        border: none;
        border-radius: 1.5rem;
        width: 70%;
        padding: 2%;
        font-weight: 600;
        color: #6c757d;
        cursor: pointer;
    }
    .proile-rating {
        font-size: 12px;
        color: #818182;
        margin-top: 5%;
    }
    .proile-rating span {
        color: #495057;
        font-size: 15px;
        font-weight: 600;
    }
    .profile-head .nav-tabs {
        margin-bottom: 5%;
    }
    .profile-head .nav-tabs .nav-link {
        font-weight: 600;
        border: none;
    }
    .profile-head .nav-tabs .nav-link.active {
        border: none;
        border-bottom: 2px solid #0062cc;
    }
    .profile-work {
        padding: 14%;
        margin-top: -15%;
    }
    .profile-work p {
        font-size: 12px;
        color: #818182;
        font-weight: 600;
        margin-top: 10%;
    }
    .profile-work a {
        text-decoration: none;
        color: #495057;
        font-weight: 600;
        font-size: 14px;
    }
    .profile-work ul {
        list-style: none;
    }
    .profile-tab label {
        font-weight: 600;
    }
    .profile-tab p {
        font-weight: 600;
        color: #0062cc;
    }
    body, html {
        margin: 10;
        padding: 10;
    }
    .app-container {
        width: 100%;
        max-width: none;
        margin: 0;
        padding: 0;
    }
    .navbar {
        width: 93%;
        position: fixed;
        top: 0;
        z-index: 1000;
    }
`;
    return (


        <>
           <style>
                {`
                .feedback-container {
                    padding: 10px;
                    margin-left: 220px; 
                }
                `}
            </style>
        
        <style>{cssStyles}</style><nav className="navbar navbar-expand-xl navbar-blue bg-blue">
            <div className="container-fluid">
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0"></ul>
                    <form className="d-flex justify-content-end">
                        <Link to="/chat">Conversations</Link>
                        <span style={{ margin: '0 10px' }}></span>
                        
                    </form>
                </div>
            </div>
        </nav>
        
        <div className="col-md-2">
                    <SideBar1 userId={userId} />
                </div>
                
                <div className='feedback-container'>
                <Container>
                {displayForm ?
                    (
                        <><Card.Header>
                                <cite title="Source Title">We are committed to providing you with the best dining experience possible, so we welcome your comments.</cite>
                            </Card.Header><Card.Body>
                                    <blockquote className="blockquote mb-0">
                                        Please fill out this questionnaire.
                                    </blockquote>
                                </Card.Body><Container className='padding30px'>
                                    <Form>
                                        <Row>
                                            <Col>
                                                <Form.Group className="mb-3" controlId="formBasicName">
                                                    <Form.Label className='required-field'> Name</Form.Label>
                                                    <Form.Control type="text" required placeholder="E.g. Jon Snow" value={clientName} onChange={e => setClientName(e.target.value)} />
                                                </Form.Group>
                                                {errors.clientName && (
                                                    <Alert variant='danger'>
                                                        &#9432;{errors.clientName}
                                                    </Alert>
                                                )}
                                            </Col>
                                            <Col>
                                                <Form.Group className="mb-3" controlId="formBasicReference">
                                                    <Form.Label className='required-field'>Request Reference</Form.Label>
                                                    <Form.Control type="text" required placeholder="Reference" value={reference} onChange={e => setReference(e.target.value)} />
                                                </Form.Group>
                                                {errors.reference && (
                                                    <Alert variant='danger'>
                                                        &#9432;{errors.reference}
                                                    </Alert>
                                                )}
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col>
                                                <Form.Group className="mb-3" controlId="formBasicComments">
                                                    <Form.Label className='required-field'>Comments</Form.Label>
                                                    <Form.Control as="textarea" rows={3} required placeholder="Your comments" value={comments} onChange={e => setComments(e.target.value)} />
                                                </Form.Group>
                                                {errors.comments && (
                                                    <Alert variant='danger'>
                                                        &#9432;{errors.comments}
                                                    </Alert>
                                                )}
                                            </Col>
                                        </Row>
                                        <Row>
                                            {Object.keys(feedbackType).map((ty) => (
                                                <React.Fragment key={ty}>
                                                    <Col>
                                                        <Form.Group className="mb-3" controlId={ty}>
                                                            <Form.Label className='required-field'>{feedbackType[ty]}</Form.Label>
                                                            <InputGroup>
                                                                <div className="mb-3">
                                                                    {feedbackOpts.map((opt, key) => (
                                                                        <Form.Check
                                                                            inline
                                                                            label={opt}
                                                                            name={`${ty}_feedback_opts`}
                                                                            id={`${ty}_${key}`}
                                                                            checked={checkedValues.some(item => item.question === ty && item.value === opt)}
                                                                            onChange={e => handleOnChange(e.target.checked, opt, ty)}
                                                                            type='checkbox'
                                                                            value={opt}
                                                                            key={key} />
                                                                    ))}
                                                                </div>
                                                            </InputGroup>
                                                        </Form.Group>
                                                        {errors[ty] && (
                                                            <Alert variant='danger'>
                                                                &#9432;{errors[ty]}
                                                            </Alert>
                                                        )}
                                                    </Col>
                                                    {(ty === 'qob' || ty === 'exp') ? <Row /> : null}
                                                </React.Fragment>
                                            ))}
                                        </Row>
                                        <Button className='btn_purp' onClick={e => formSubmit(e)}>Submit Review</Button>
                                    </Form>
                                </Container></>
                    
                    ) : (
                        <Card bg='light' text='dark'>
                            <Card.Body>
                                <div className='padding30px'>
                                    <div className="circle">
                                        <div className="checkmark"></div>
                                    </div>
                                </div>
                                <Card.Text>
                                    Thank you for providing the feedback
                                </Card.Text>
                                <Form.Text muted>
                                    We will work towards improving your experience
                                </Form.Text>
                                <div className='padding30px'>
                                    <Button className='btn_purp' onClick={() => window.location.href = `/submissions1/${userId}`}>Close</Button>
                                </div>
                            </Card.Body>
                        </Card>
                    )}



            </Container>
            </div>
            </>
    );
}

export default FeedbackForm1;
