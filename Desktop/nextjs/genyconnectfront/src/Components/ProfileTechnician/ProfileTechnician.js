import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import { AiFillMessage } from "react-icons/ai";
import { useParams } from 'react-router-dom';

import SideBar from '../profile/SideBar';
import SideBar2 from '../profile/Sidebar2';
import { AiOutlineMessage } from "react-icons/ai";

import { IoIosNotificationsOutline } from "react-icons/io";

const ProfileTechnician = () => {
    const [userProfile, setUserProfile] = useState({});
    const {userId} = useParams(); // Utilisez useParams pour récupérer l'userId de l'URL

    useEffect(() => {
        fetchUserProfile();
    }, []);

    const fetchUserProfile = async () => {
        try {
            const response = await fetch(`http://localhost:8080/api/v1/users/${userId}`, {
                method: 'GET', 
                // Assurez-vous d'ajouter les en-têtes d'authentification nécessaires
                // par exemple, les jetons JWT si vous utilisez une authentification basée sur les jetons.
            });
            if (!response.ok) {
                throw new Error('Erreur lors de la récupération du profil utilisateur');
            }
            const userData = await response.json();
            setUserProfile(userData);
        } catch (error) {
            console.error('Erreur:', error);
            // Gérer les erreurs de récupération de profil ici
        }
    };

    // Affichez les données du profil une fois qu'elles sont récupérées
    console.log(userProfile);
    console.log(userId);

    // Reste du code pour afficher les données du profil dans l'interface utilisateur

    const handleLogoutHover = (event) => {
        // Change la couleur du texte en bleu lorsque le curseur survole le mot "Logout"
        event.target.style.color = '#2E86C1';
    };
    
    const handleLogoutLeave = (event) => {
        // Rétablit la couleur du texte par défaut lorsque le curseur quitte le mot "Logout"
        event.target.style.color = 'initial';
    };
   
    const profileStyles = `
    .profile-container {
        padding: 20px;
    }

    .profile-card {
        background-color: #fff;
        border-radius: 10px;
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        padding: 20px;
        margin-bottom: 20px;
    }

    .profile-card h5 {
        margin-bottom: 10px;
        color: #333;
    }

    .profile-card h6 {
        color: #555;
    }

    .profile-details {
        margin-bottom: 20px;
    }

    .profile-details p {
        margin-bottom: 10px;
    }

    .profile-header {
        display: flex;
        align-items: center;
    }

    .profile-picture {
        width: 100px;
        height: 100px;
        border-radius: 50%;
        overflow: hidden;
        margin-right: 20px;
    }

    .profile-picture img {
        width: 100%;
        height: 100%;
        object-fit: cover;
    }
    `;

    const cssStyles = `
    
.emp-profile{
    padding: 3%;
    margin-top: 3%;
    margin-bottom: 3%;
    border-radius: 0.5rem;
    background: #fff;
}
.profile-img{
    text-align: center;
}
.profile-img img{
    width: 70%;
    height: 100%;
}
.profile-img .file {
    position: relative;
    overflow: hidden;
    margin-top: -20%;
    width: 70%;
    border: none;
    border-radius: 0;
    font-size: 15px;
    background: #212529b8;
}
.profile-img .file input {
    position: absolute;
    opacity: 0;
    right: 0;
    top: 0;
}
.profile-head h5{
    color: #333;
}
.profile-head h6{
    color: #0062cc;
}
.profile-edit-btn{
    border: none;
    border-radius: 1.5rem;
    width: 70%;
    padding: 2%;
    font-weight: 600;
    color: #6c757d;
    cursor: pointer;
}
.proile-rating{
    font-size: 12px;
    color: #818182;
    margin-top: 5%;
}
.proile-rating span{
    color: #495057;
    font-size: 15px;
    font-weight: 600;
}
.profile-head .nav-tabs{
    margin-bottom:5%;
}
.profile-head .nav-tabs .nav-link{
    font-weight:600;
    border: none;
}
.profile-head .nav-tabs .nav-link.active{
    border: none;
    border-bottom:2px solid #0062cc;
}
.profile-work{
    padding: 14%;
    margin-top: -15%;
}
.profile-work p{
    font-size: 12px;
    color: #818182;
    font-weight: 600;
    margin-top: 10%;
}
.profile-work a{
    text-decoration: none;
    color: #495057;
    font-weight: 600;
    font-size: 14px;
}
.profile-work ul{
    list-style: none;
}
.profile-tab label{
    font-weight: 600;
}
.profile-tab p{
    font-weight: 600;
    color: #0062cc;
}
body, html {
    margin: 10;
    padding: 10;
}
    .app-container {
        width:100%; /* Par défaut, prendra toute la largeur de son conteneur parent */
        max-width: none; /* Assurez-vous qu'il n'y a pas de limite de largeur maximale */
        margin: 0 /* Supprime toutes les marges */
        padding: 0; /* Supprime tous les paddings */
      }
      .navbar {
        width: 93%; /* Définit la largeur de la navbar à 100% */
        position: fixed; /* Fixe la navbar en haut de la page */
        top: 0; /* Place la navbar en haut de la page */
        z-index: 1000; 
    }


    `;
return (
     

    <><div className="col-md-2">
        <SideBar2 userId={userId}  />
    </div><>
            <style>{cssStyles}</style>
            <nav class="navbar navbar-expand-xl navbar-blue bg-blue">
            <div class="container-fluid">



                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    </ul>
                    <form class="d-flex">
                    <Link to="/chat">
Conversations
    </Link>
    <span style={{ margin: '0 10px' }}></span>



                    </form>
                </div>
            </div>
        </nav>
                                 
        <div className="container-fluid profile-container">
            <div className="row justify-content-center">
                <div className="col-md-8 profile-card">
                    <div className="profile-header">

                       
                    </div>
                    <div className="profile-details">
                     <p>FirstName: {userProfile.firstname}</p>
                        <p>LastName: {userProfile.lastname}</p>

                        <p>Email: {userProfile.email}</p>
                        <p>phone Number: {userProfile.phoneNumber}</p>
                        <p>Adress: {userProfile.adress}</p>
                        <p>Business Name: {userProfile.businessName}</p>


                    </div>
                </div>
            </div>
        </div><style>{profileStyles}</style></>


        </>
    );
}
export default ProfileTechnician