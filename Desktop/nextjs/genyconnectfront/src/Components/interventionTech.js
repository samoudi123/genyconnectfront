import React , {useState, useEffect} from "react";
import axios from "axios";
import { Link, useParams } from 'react-router-dom';
import SideBar2 from "./profile/Sidebar2";
import intervention from "./Assets/inter.png"


function InterventionTech(){
    const { userId } = useParams(); // Récupérez clientId à partir des paramètres d'URL
const [userData, setUserData] = useState([]);

    const Endpoint = `http://localhost:8080/api/request/technician/${userId}`;

    const getUserData = async () => {
        try {
            const fetchData = await axios.get(Endpoint);
            setUserData(fetchData.data);
       

        } catch (error) {
            console.log(error);
        }
    };

    useEffect(() => {
        getUserData();
    }, [userId]);
    const buttonContainerStyle = {
        display: 'flex',
        justifyContent: 'flex-end',
        marginTop: '20px',
      };
      const listItemStyle = {
        marginTop: '70px', // Ajouter une marge supérieure
    };
    
      const customButtonStyle = {
        backgroundColor: '#176184',
        fontWeight: 'bold',

        color: 'white',
        padding: '10px 20px',
        border: 'none',
        borderRadius: '5px',
        
        cursor: 'pointer',
      };
      const table=  {
        color: ' #0DA2B4' ,
    };
    const cssStyles = `
    
    .emp-profile{
        padding: 3%;
        margin-top: 3%;
        margin-bottom: 3%;
        border-radius: 0.5rem;
        background: #fff;
    }
    .profile-img{
        text-align: center;
    }
    .profile-img img{
        width: 70%;
        height: 100%;
    }
    .profile-img .file {
        position: relative;
        overflow: hidden;
        margin-top: -20%;
        width: 70%;
        border: none;
        border-radius: 0;
        font-size: 15px;
        background: #212529b8;
    }
    .profile-img .file input {
        position: absolute;
        opacity: 0;
        right: 0;
        top: 0;
    }
    .profile-head h5{
        color: #333;
    }
    .profile-head h6{
        color: #0062cc;
    }
    .profile-edit-btn{
        border: none;
        border-radius: 1.5rem;
        width: 70%;
        padding: 2%;
        font-weight: 600;
        color: #6c757d;
        cursor: pointer;
    }
    .proile-rating{
        font-size: 12px;
        color: #818182;
        margin-top: 5%;
    }
    .proile-rating span{
        color: #495057;
        font-size: 15px;
        font-weight: 600;
    }
    .profile-head .nav-tabs{
        margin-bottom:5%;
    }
    .profile-head .nav-tabs .nav-link{
        font-weight:600;
        border: none;
    }
    .profile-head .nav-tabs .nav-link.active{
        border: none;
        border-bottom:2px solid #0062cc;
    }
    .profile-work{
        padding: 14%;
        margin-top: -15%;
    }
    .profile-work p{
        font-size: 12px;
        color: #818182;
        font-weight: 600;
        margin-top: 10%;
    }
    .profile-work a{
        text-decoration: none;
        color: #495057;
        font-weight: 600;
        font-size: 14px;
    }
    .profile-work ul{
        list-style: none;
    }
    .profile-tab label{
        font-weight: 600;
    }
    .profile-tab p{
        font-weight: 600;
        color: #0062cc;
    }
    body, html {
        margin: 10;
        padding: 10;
    }
        .app-container {
            width:100%; /* Par défaut, prendra toute la largeur de son conteneur parent */
            max-width: none; /* Assurez-vous qu'il n'y a pas de limite de largeur maximale */
            margin: 0 /* Supprime toutes les marges */
            padding: 0; /* Supprime tous les paddings */
          }
          .navbar {
            width: 93%; /* Définit la largeur de la navbar à 100% */
            position: fixed; /* Fixe la navbar en haut de la page */
            top: 0; /* Place la navbar en haut de la page */
            z-index: 1000; 
        }
    
    
        `;
    return( 
        <div className="container">
            <div className="row">
                <div className="col-md-2">
                <SideBar2 userId={userId}/>
                </div>
                <style>{cssStyles}</style>
            <nav class="navbar navbar-expand-xl navbar-blue bg-blue">
            <div class="container-fluid">



                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    </ul>
                    <form className="d-flex justify-content-end">
                            <Link to="/chat">Conversations</Link>
                            <span style={{ margin: '0 10px' }}></span>
                            
                        </form>
                </div>
            </div>
        </nav>
          <div className="col-md-10">
                    {userData.length === 0 ? (
                        <div className="col-md-10" style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
                        <h2>No request found</h2>
                        </div>
                    ) :(
                     userData.map((item) => (
                        <li className="card p-3 mb-2" key={item.id} style={listItemStyle}>
                            <div className="coard-body">
                                <p className="card-text d-flex justify-content-between align-items-center"> 
                                <Link to={`/detaills/${item.reference}`}>
                                <img src={intervention} alt="Intervention Icon" style={{ width: '40px', marginRight: '10px', filter: 'invert(26%) sepia(73%) saturate(6545%) hue-rotate(185deg) brightness(93%) contrast(102%)' }} />
{item.reference}</Link>


                                <a href="#"  style={table} >
                                <Link to={`/uploadD/${item.id}`}>


<button type="button" class="btn btn-outline-success m-2  " disabled={!!item.fileName }>upload diagnostic sheet here</button>

</Link>
<Link to={`/timeline1/${item.reference}`}>

<button type="button" class="btn btn-outline-success m-2  "disabled={item.status === 'terminé' || (item.status !== 'en_cours'   || !item.fileName1 || !item.fileName)}>
In progress</button>

</Link>
                                


                               

                                        </a>
                                </p>
                                
                            </div>
                        </li>
                    ))
                )}
                      </div>
            </div>
        </div>
    );
}

export default InterventionTech;
