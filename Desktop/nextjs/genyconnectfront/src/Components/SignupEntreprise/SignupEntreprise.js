import React from 'react';
import { useState } from 'react';

import logoLeft from '../Assets/Fichier 7 (1).png'; // Importez votre logo à gauche
import 'react-phone-number-input/style.css'
import axios from 'axios';
import photo from '../Assets/1.png';

import { useLocation } from 'react-router-dom';
import PhoneInput from 'react-phone-number-input'

import {
  MDBBtn,
  MDBContainer,
  MDBCard,
  MDBCardBody,
  MDBInput,
  MDBIcon,
  MDBRow,
  MDBCol,
  MDBCheckbox
}
from 'mdb-react-ui-kit';

  function SignupEntreprise() {
    const location = useLocation();
    const queryParams = new URLSearchParams(location.search);
    const role = queryParams.get('role');
    const [firstname, setFirstname] = useState('');
    const [lastname, setLastname] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [adress, setAdress] = useState('');
    const [businessName, setBusinessName] = useState('');
    const [workSector, setWorkSector] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');
  
    
    const saveEntreprise = async (event) => {
      event.preventDefault();
      try {
        await axios.post('http://localhost:8080/api/v1/auth/register', {
          firstname: firstname,
          lastname: lastname,
          email: email,
          password: password,
          adress: adress,
          businessName: businessName,
          workSector: workSector,
          phoneNumber: phoneNumber,
          role: role || 'ENTREPRISE'
        });
        alert('Client Registration Successfully');
      } catch (error) {
        alert(error);
      }
    };
    const cssStyles = `
    .cascading-right {
      margin-right: -40px;
    }
    
    @media (max-width: 900px) {
      .cascading-right {
        margin-right: 1;
      }
    }
    .logo {
      position: absolute;
      top: -100px;
      left: 50px;
      width: 200px; /* Adjust the size of the logo */
      height: auto;
      z-index: 1;
    }
    `;




  return (
    <>
      <style>{cssStyles}</style>
  <div style={{ position: 'absolute', top: '200px', left: '300px' }}>
  <img className="logo" src={logoLeft} alt="Logo" />
</div>      <MDBContainer fluid className='my-2'>
        <MDBRow className='g-3 align-items-center justify-content-center' style={{ minHeight: '100vh' }}>
          <MDBCol md='5'> {/* Utilisation de col-md-6 pour contrôler la taille du conteneur */}
            <MDBCard className='my-5 cascading-right' style={{ background: 'hsla(0, 0%, 100%, 0.55)', backdropFilter: 'blur(30px)' }}>
              <MDBCardBody className='p-5 shadow-5 text-center'>
                <h2 className="fw-bold mb-5"></h2>
                <MDBRow>
                  <MDBCol col='6'>
                    <MDBInput wrapperClass='mb-4' label='FirstName' id='form1' type='text'  value={firstname} onChange={(e) => setFirstname(e.target.value)} />
                  </MDBCol>
                  <MDBCol col='6'>
                    <MDBInput wrapperClass='mb-4' label='LastName' id='form2' type='text'  value={lastname} onChange={(e) => setLastname(e.target.value)} />
                  </MDBCol>
                </MDBRow>
                <MDBInput wrapperClass='mb-4' label='Email' id='form3' type='email'  value={email} onChange={(e) => setEmail(e.target.value)} />
                <MDBInput wrapperClass='mb-4' label='Password' id='form4' type='password'  value={password} onChange={(e) => setPassword(e.target.value)} />
                <MDBInput wrapperClass='mb-4' label='Adress' id='form5' type='text'  value={adress} onChange={(e) => setAdress(e.target.value)} />
                <MDBInput wrapperClass='mb-4' label='Business Name' id='form6' type='text'   value={businessName} onChange={(e) => setBusinessName(e.target.value)}/>
                <MDBInput wrapperClass='mb-4' label='Work Sector' id='form7' type='text'   value={workSector} onChange={(e) => setWorkSector(e.target.value)}/>
                <PhoneInput
      placeholder="Enter phone number"
      value={phoneNumber}
      onChange={setPhoneNumber}/>
                <div className='d-flex justify-content-center mb-4'>
                </div>
                <MDBBtn className='w-100 mb-4' size='md' onClick={saveEntreprise}>Sign up</MDBBtn>
                <div className="text-center"></div>
              </MDBCardBody>
            </MDBCard>
          </MDBCol>
          <MDBCol md='6'>
            <img src={photo} className="w-100 rounded-4 shadow-4" alt="" fluid />
          </MDBCol>
        </MDBRow>
      </MDBContainer>
    </>
  );
}

export default SignupEntreprise;
