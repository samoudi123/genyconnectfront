import { Image } from 'react-bootstrap';
import image from '../Assets/Fichier 1 (1).png';
import logo1 from '../Assets/Fichier 7 (1).png';
import { Link } from 'react-router-dom';

// Styles pour l'image de fond
const backgroundImageStyle = `
  position: fixed;
  top:0 ;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: -1;
  object-fit: cover;
`;

// Styles pour le logo
const logoStyle = `
  width: 400px;
  height: 120px;
  position: absolute;
  top: 250px;
  right: 50px;
  left: 950px;
  z-index: 10;
`;

// Styles pour la navigation
const navStyle = `
display: flex;
justify-content: left;
margin-left: 800px; 
margin-top:15px;// Ajoutez une marge à gauche pour l'espacement
`;
const titleContainerStyle = `
  position: absolute;
  top: 47%; /* Centre le conteneur verticalement */
  right: -1010px; /* Ajuste la distance par rapport à la droite */
  text-align: left; /* Aligne le texte à gauche */
  width: 100%; /* Utilise toute la largeur */
  padding-bottom: 8px; /* Ajoute de l'espace en bas */
  color: #58B0D1; /* Couleur du texte */
`;
const titleContainerStyle1 = `
  position: absolute;
  top: 60%; /* Centre le conteneur verticalement */
  right: -1010px; /* Ajuste la distance par rapport à la droite */
  text-align: left; /* Aligne le texte à gauche */
  width: 100%; /* Utilise toute la largeur */
  padding-bottom: 8px; /* Ajoute de l'espace en bas */
  color: #B3DEE8; /* Couleur du texte */
`;


const boldTitleStyle = `
  font-size:1.9rem; /* Taille du texte */
`;
const boldTitleStyle1 = `
  font-weight: bold; /* Texte en gras */
  font-size:1.8rem; /* Taille du texte */
`;
export default function Home() {
  return (
    <>
      <style>
        {/* Styles pour l'image de fond */}
        {`.background-image { ${backgroundImageStyle} }`}
        
        {/* Styles pour le logo */}
        {`.logo { ${logoStyle} }`}
        
        {/* Styles pour la navigation */}
        {`.custom-nav { ${navStyle} }`}
        {`.title-container { ${titleContainerStyle} }`}
        {`.boldTitleStyle { ${boldTitleStyle} }`}
        {`.title-container1 { ${titleContainerStyle1} }`}
        {`.boldTitleStyle1 { ${boldTitleStyle1} }`}



      </style>

      <Image
        src={image}
        alt="..."
        className="background-image"
      />
      <div className="absolute">
        <Image
          src={logo1}
          alt="Logo"
          className='logo'
        />
        
      </div>
      
      <nav className="custom-nav">
        <div className="max-w-7xl mx-auto px-5 sm:px-7 lg:px-9 ">
          <div className="flex items-center justify-between h-16">
            <div className="flex items-center"></div>
            <div className="hidden md:block">
              <div className="mr-6 flex items-center space-x-4 text-blue-200">
                <a href="/" className="text-white hover:bg-white hover:text-black rounded-lg p-2 me-2 ">Home</a>
                <a href="/" className="text-white hover:bg-white hover:text-black rounded-lg p-2 me-2">About Us</a>
                <a href="/" className="text-white hover:bg-white hover:text-black rounded-lg p-2 me-2">Contact Us</a>
                <Link to="/login" className="btn btn-success ml-4">
        Get Started
      </Link>
              </div>
            </div>
          </div>
        </div>
      </nav>
      <div className="title-container">
        <h1 className=" boldTitleStyle ">quality is at the core of everything</h1>
        <h1 className="  boldTitleStyle"> we do at GenyConncet</h1>
    </div>
    <div className="title-container1">

        <h1 className=" boldTitleStyle1 "> our mission is to provide</h1>
        <h1 className=" boldTitleStyle1"> maintenance and repair services</h1>
        <h1 className=" boldTitleStyle1 "> for medical devices of the highest</h1>
        <h1 className=" boldTitleStyle1"> quality, ensuring the reliability and</h1>
        <h1 className=" boldTitleStyle1"> safety of equipment used in the</h1>
        <h1 className=" boldTitleStyle1"> healthcare sector.</h1>

      </div>
    </>
  );
}
