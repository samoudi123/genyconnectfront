import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Stomp from 'stompjs';
import SockJS from 'sockjs-client';
import { Box, Card, CardContent, MenuList, MenuItem, ListItemText, IconButton, TextField, Button, Typography, Divider, Avatar, Menu } from '@material-ui/core';
import { useNavigate, useParams } from 'react-router-dom';
import back1 from '../Assets/back1.jpg';
import '../Chat/ChatRoom.css';

const ChatRoom = () => {
    const { userId } = useParams();
    const [isSending, setIsSending] = useState(false);
    const [messages, setMessages] = useState([]);
    const [messageInput, setMessageInput] = useState('');
    const [users, setUsers] = useState([]);
    const [userClick, setUserClick] = useState(null);
    const [stompClient, setStompClient] = useState(null);
    const [open, setOpen] = useState(false);
    const [anchorEl, setAnchorEl] = useState(null);
    const isMenuOpen = Boolean(anchorEl);
    const menuId = 'primary-search-account-menu';
    const [currentUser, setCurrentUser] = useState(localStorage.getItem('currentUser'));
    const [currentUserDetails, setCurrentUserDetails] = useState(null);
    const navigate = useNavigate();

    const handleRedirect = () => {
        navigate('/');
    };

    const handleLogOut = () => {
        localStorage.clear();
        navigate('/');
    };

    useEffect(() => {
        const fetchCurrentUserDetails = async () => {
            try {
                const accessToken = localStorage.getItem('accessToken');
                const userDetailsResponse = await axios.get('http://localhost:8080/api/v1/users/currentUser', {
                    headers: {
                        Authorization: `Bearer ${accessToken}`,
                    },
                });

                const currentUserDetails = userDetailsResponse.data;
                setCurrentUserDetails(currentUserDetails);
            } catch (error) {
                console.error('Failed to fetch user details', error);
            }
        };

        fetchCurrentUserDetails();
    }, []);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const accessToken = localStorage.getItem('accessToken');
                const userDetailsResponse = await axios.get('http://localhost:8080/api/v1/users/currentUser', {
                    headers: {
                        Authorization: `Bearer ${accessToken}`,
                    },
                });

                const currentUserDetails = userDetailsResponse.data;
                if (currentUserDetails.role === 'CLIENT') {
                    const response = await axios.get(`http://localhost:8080/api/request/client/${currentUserDetails.id}/technicians`);
                    setUsers(response.data);
                } else if (currentUserDetails.role === 'TECHNICIAN') {
                    const response = await axios.get(`http://localhost:8080/api/request/technician/${currentUserDetails.id}/clients`);
                    setUsers(response.data);
                } else {
                    console.error('Unknown user role:', currentUserDetails.role);
                }
            } catch (error) {
                console.error('Error fetching users:', error);
            }
        };

        fetchData();
    }, []);

    const handleProfileMenuOpen = (event) => {
        setAnchorEl(event.currentTarget);
        setOpen(!open);
    };

    const handleMenuClose = () => {
        setAnchorEl(null);
        setOpen(false);
    };

    useEffect(() => {
        const connect = () => {
            const url = 'http://localhost:8080/ws';
            const socket = new SockJS(url);
            const stompClient = Stomp.over(socket);
            setStompClient(stompClient);

            stompClient.connect({}, (frame) => {
                console.log(`Connected to server ${frame}`);
                stompClient.subscribe(`/topic/${currentUser}`, (message) => {
                    showMessage(JSON.parse(message.body));
                });
            }, (error) => {
                console.error('Connection error:', error);
            });
        };

        connect();

        return () => {
            if (stompClient) {
                stompClient.disconnect();
            }
        };
    }, [currentUser]);

    const formatMessageDate = (dateString) => {
        const date = new Date(dateString);
        return `${date.getHours()}:${('0' + date.getMinutes()).slice(-2)}`;
    };
  
    
    const showMessage = (msg) => {
        if (msg && msg.content) {
            const messageExists = messages.some((message) => message.dateMessage === msg.dateMessage);
            if (!messageExists) {
                // Détecter les liens de réunion dans le contenu du message
                const meetingLinkPattern = /http:\/\/localhost:3000\/room\/(.+)/;
                const match = msg.content.match(meetingLinkPattern);
                if (match && match[1]) {
                    // Si un lien de réunion est trouvé, extrayez l'ID de la réunion
                    const roomID = match[1];
                    // Utilisez meetingRoomID pour rediriger l'utilisateur vers la réunion
                    const messageWithLink = (
                        <span style={{color:'#ffff'}} dangerouslySetInnerHTML={{ __html: msg.content.replace(meetingLinkPattern, `<a href="/room/${roomID}">http://localhost:3000/room/${roomID}</a>`) }}></span>
                    );
                    setMessages((prevMessages) => [
                        ...prevMessages,
                        {
                            sender: msg.sender,
                            content: messageWithLink,
                            rec: msg.rec,
                            dateMessage: msg.dateMessage,
                            lu: false,
                        },
                    ]);
                } else {
                    // Si aucun lien de réunion n'est trouvé, ajoutez simplement le message à la liste des messages
                    setMessages((prevMessages) => [
                        ...prevMessages,
                        {
                            sender: msg.sender,
                            content: msg.content,
                            rec: msg.rec,
                            dateMessage: msg.dateMessage,
                            lu: false,
                        },
                    ]);
                }
            }
        }
    };
    
    
        

    const sendMessage = () => {
        if (!isSending && stompClient) {
            setIsSending(true);
            const dateMessage = new Date();
            if (userClick && userClick.id) {
                const chatMessage = {
                    sender: currentUser,
                    rec: userClick.id,
                    content: messageInput,
                    type: 'CHAT',
                    dateMessage: dateMessage.getTime(),
                };

                stompClient.send('/app/chat.sendMessage', {}, JSON.stringify(chatMessage));
                setMessageInput('');
                setIsSending(false);
            }
        }
    };

    const handleChange = (event) => {
        setMessageInput(event.target.value);
    };

    const clickUser = async (user) => {
        try {
            setUserClick(user);
            if (currentUser && currentUser.trim() !== '') {
                setMessages([]);
                const SenderId = currentUser;
                const RecId = user.id;
                const response = await axios.get(`http://localhost:8080/conversation/${SenderId}/${RecId}`);
                setMessages(response.data);
            }
        } catch (error) {
            console.error('Erreur lors de la récupération de la conversation:', error);
        }
    };

    const renderMenu = (
        <Menu
            anchorEl={anchorEl}
            anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
            id={menuId}
            keepMounted
            transformOrigin={{ vertical: 'top', horizontal: 'right' }}
            open={isMenuOpen}
            onClose={handleMenuClose}
        >
            <Typography style={{ padding: '5px', textAlign: 'center', fontWeight: 'bold', color: 'red' }}>
                <span>USER COORDINATES</span>
            </Typography>
            <Divider />
            <Typography variant="body2" color="textSecondary" style={{ padding: '15px', backgroundColor: `#62C2E2` }}>
                {currentUserDetails && (
                    <div>
                        <Avatar
                            alt=""
                            style={{ width: '75px', height: '75px', cursor: 'pointer', margin: '0 auto 10px' }}
                        />
                        <p style={{ color: '#fff' }}>Email: {currentUserDetails.email}</p>
                        <p style={{ color: '#fff' }}>User Name: {currentUserDetails.lastname} {currentUserDetails.firstname}</p>
                    </div>
                )}
            </Typography>
            <Divider />
            <MenuItem onClick={handleLogOut}>LogOut</MenuItem>
        </Menu>
    );

    return (
        <div
    className="chat-container"
    style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100vh',
        backgroundColor: '#62C2E2'  // Change this color to match the background of the technician list
    }}
        >
            <div>
                <Box
                    sx={{ position: 'fixed', top: 0, right: 0, padding: '10px', zIndex: 9999 }}
                >
                    {currentUserDetails && (
                        <div>
                            <Avatar
                                alt=""
                                size="large"
                                edge="end"
                                aria-label="account of current user"
                                aria-haspopup="true"
                                style={{ width: '47px', height: '47px', cursor: 'pointer' }}
                                aria-controls={menuId}
                                onClick={handleProfileMenuOpen}
                                color="inherit"
                            />
                        </div>
                    )}
                </Box>
            </div>
            {renderMenu}
            <div>
                <Card className="user-list-card" style={{ margin: '0 20px' }}>
                    <div
                        className="user-list-overlay"
                        style={{
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            right: 0,
                            bottom: 0,
                            backgroundColor: `#62C2E2`
                        }}
                    ></div>
                    <CardContent
                        style={{
                            height: '650px',
                            width: '250px',
                            backgroundColor: 'rgba(255, 255, 255, 0.5)',
                            padding: '20px',
                            position: 'relative',
                            zIndex: 1,
                        }}
                    >
                        <div>
                            {currentUserDetails && currentUserDetails.role === 'CLIENT' ? (
                                <Typography variant="h5" component="h2">
                                    Technicians Available
                                </Typography>
                            ) : (
                                <Typography variant="h5" component="h2">
                                    Clients Available
                                </Typography>
                            )}
                        </div>
                        <div style={{ maxHeight: '530px', overflowY: 'auto' }}>
                            <MenuList>
                                {Array.isArray(users) && users.length > 0 ? (
                                    users.map((user) => (
                                        <MenuItem key={user.id} onClick={() => clickUser(user)}>
                                            <ListItemText>
                                                <div style={{ display: 'flex', alignItems: 'center' }}>
                                                    <i style={{ fontSize: '24px' }} className="fa">&#xf007;</i>
                                                    <span style={{ marginLeft: '10px' }}>
                                                        {`${user.firstname} ${user.lastname}`}
                                                    </span>
                                                </div>
                                            </ListItemText>
                                            {currentUserDetails && currentUserDetails.role === 'CLIENT' && <ListItemText />}
                                        </MenuItem>
                                    ))
                                ) : (
                                    <MenuItem>
                                        <ListItemText>
                                            <div>
                                                <i style={{ fontSize: '24px' }} className="fa">&#xf007;</i>
                                                <span>No user available</span>
                                            </div>
                                        </ListItemText>
                                    </MenuItem>
                                )}
                            </MenuList>
                        </div>
                        <div>
                            <IconButton variant="contained" onClick={handleRedirect} style={{ backgroundColor: '#b21be8' }}>
                                {/* Icon can be added here */}
                            </IconButton>
                        </div>
                    </CardContent>
                </Card>
            </div>

            <div>
                <Card className="chat-content-card" style={{ margin: '0 20px', position: 'relative' }}>
                    <div
                        className="chat-content-overlay"
                        style={{
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            right: 0,
                            bottom: 0,
                            backgroundSize: 'cover',
                            backgroundPosition: 'center',
                            opacity: 1.1,
                        }}
                    ></div>

                    <CardContent
                        style={{
                            height: '650px',
                            width: '850px',
                            backgroundColor: 'rgba(255, 255, 255, 0.5)',
                            padding: '20px',
                            position: 'relative',
                            zIndex: 1,
                        }}
                    >
                        <Typography variant="h6">
                            {userClick && (
                                <div style={{ display: 'flex', alignItems: 'center' }}>
                                    <Avatar
                                        alt=""
                                        style={{ width: '35px', height: '35px', cursor: 'pointer' }}
                                        size="large"
                                        edge="end"
                                        aria-label="account of current user"
                                    />
                                    <Typography variant="h6" style={{ marginLeft: '10px' }}>
                                        {`${userClick.firstname} ${userClick.lastname}`}
                                    </Typography>
                                </div>
                            )}
                        </Typography>

                        <div className="chat-messages" style={{ height: '500px', width: '800px', overflowY: 'auto' }}>
                            {messages.map((message) => (
                                <div key={message.id} className={`message-container ${message.sender === currentUser ? 'sent' : 'received'}`}>
                                    <div className="message-bubble message-content">
                                        {message.sender !== currentUser && (
                                            <div className="avatar">
                                                <Avatar alt="" style={{ width: '35px', height: '35px', cursor: 'pointer' }} />
                                            </div>
                                        )}
                                        <div style={{ marginBottom: '5px' }}>
                                            <p>{message.content}</p>
                                        </div>
                                        <div className="timestamp">{formatMessageDate(message.dateMessage)}</div>
                                    </div>
                                </div>
                            ))}
                        </div>

                        <div className="chat-input" style={{ display: 'flex', alignItems: 'center' }}>
                            <TextField
                                label="Enter your message..."
                                variant="outlined"
                                fullWidth
                                margin="normal"
                                value={messageInput}
                                onChange={handleChange}
                                style={{ width: '800px', marginRight: '10px' }}
                            />
                            <Button onClick={sendMessage} variant="contained" style={{ backgroundColor: '#58B0D1' }}>
                                Send
                            </Button>
                        </div>
                    </CardContent>
                </Card>
            </div>
        </div>
    );
};

export default ChatRoom;
