import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import $ from 'jquery';

function Calls() {
  const navigate = useNavigate();
  const [userData, setUserData] = useState([]);
  const [currentUserDetails, setCurrentUserDetails] = useState(null);

  useEffect(() => {
    const fetchCurrentUserDetails = async () => {
      try {
        const accessToken = localStorage.getItem('accessToken');
        const userDetailsResponse = await axios.get('http://localhost:8080/api/v1/users/currentUser', {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        });

        const currentUserDetails = userDetailsResponse.data;
        setCurrentUserDetails(currentUserDetails);
      } catch (error) {
        console.error('Failed to fetch user details', error);
      }
    };

    fetchCurrentUserDetails();
  }, []);

  useEffect(() => {
    if (currentUserDetails) {
      getUserData();
    }
  }, [currentUserDetails]);

  const getUserData = async () => {
    try {
      const Endpoint = `http://localhost:8080/api/request/client/${currentUserDetails.id}/technicians`;
      const fetchData = await axios.get(Endpoint);
      setUserData(fetchData.data);
    } catch (error) {
      console.log(error);
    }
  };

  const handleContactClick = (userId) => {
    navigate(`/room/${userId}`, { state: { username: "client" } });
  };
  const cssStyles=`body{
    background:#f5f5f5;
    margin-top:20px;}

/* ===== Career ===== */
.career-form {
  background-color: #4e63d7;
  border-radius: 5px;
  padding: 0 16px;
}

.career-form .form-control {
  background-color: rgba(255, 255, 255, 0.2);
  border: 0;
  padding: 12px 15px;
  color: #fff;
}

.career-form .form-control::-webkit-input-placeholder {
  /* Chrome/Opera/Safari */
  color: #fff;
}

.career-form .form-control::-moz-placeholder {
  /* Firefox 19+ */
  color: #fff;
}

.career-form .form-control:-ms-input-placeholder {
  /* IE 10+ */
  color: #fff;
}

.career-form .form-control:-moz-placeholder {
  /* Firefox 18- */
  color: #fff;
}

.career-form .custom-select {
  background-color: rgba(255, 255, 255, 0.2);
  border: 0;
  padding: 12px 15px;
  color: #fff;
  width: 100%;
  border-radius: 5px;
  text-align: left;
  height: auto;
  background-image: none;
}

.career-form .custom-select:focus {
  -webkit-box-shadow: none;
          box-shadow: none;
}

.career-form .select-container {
  position: relative;
}

.career-form .select-container:before {
  position: absolute;
  right: 15px;
  top: calc(50% - 14px);
  font-size: 18px;
  color: #ffffff;
  content: '\F2F9';
  font-family: "Material-Design-Iconic-Font";
}

.filter-result .job-box {
background:#fff;
  -webkit-box-shadow: 0 0 35px 0 rgba(130, 130, 130, 0.2);
          box-shadow: 0 0 35px 0 rgba(130, 130, 130, 0.2);
  border-radius: 10px;
  padding: 10px 35px;
}

ul {
  list-style: none; 
}

.list-disk li {
  list-style: none;
  margin-bottom: 12px;
}

.list-disk li:last-child {
  margin-bottom: 0;
}

.job-box .img-holder {
  height: 65px;
  width: 65px;
  background-color: #4e63d7;
  background-image: -webkit-gradient(linear, left top, right top, from(rgba(78, 99, 215, 0.9)), to(#5a85dd));
  background-image: linear-gradient(to right, rgba(78, 99, 215, 0.9) 0%, #5a85dd 100%);
  font-family: "Open Sans", sans-serif;
  color: #fff;
  font-size: 22px;
  font-weight: 700;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  border-radius: 65px;
}

.career-title {
  background-color: #4e63d7;
  color: #fff;
  padding: 15px;
  text-align: center;
  border-radius: 10px 10px 0 0;
  background-image: -webkit-gradient(linear, left top, right top, from(rgba(78, 99, 215, 0.9)), to(#5a85dd));
  background-image: linear-gradient(to right, rgba(78, 99, 215, 0.9) 0%, #5a85dd 100%);
}

.job-overview {
  -webkit-box-shadow: 0 0 35px 0 rgba(130, 130, 130, 0.2);
          box-shadow: 0 0 35px 0 rgba(130, 130, 130, 0.2);
  border-radius: 10px;
}

@media (min-width: 992px) {
  .job-overview {
    position: -webkit-sticky;
    position: sticky;
    top: 70px;
  }
}

.job-overview .job-detail ul {
  margin-bottom: 28px;
}

.job-overview .job-detail ul li {
  opacity: 0.75;
  font-weight: 600;
  margin-bottom: 15px;
}

.job-overview .job-detail ul li i {
  font-size: 20px;
  position: relative;
  top: 1px;
}

.job-overview .overview-bottom,
.job-overview .overview-top {
  padding: 35px;
}

.job-content ul li {
  font-weight: 600;
  opacity: 0.75;
  border-bottom: 1px solid #ccc;
  padding: 10px 5px;
}

@media (min-width: 768px) {
  .job-content ul li {
    border-bottom: 0;
    padding: 0;
  }
}

.job-content ul li i {
  font-size: 20px;
  position: relative;
  top: 1px;
}

.mb-30 {
    margin-bottom: 30px;}`;


    useEffect(() => {
      $(document).ready(function() {
          $('.profile-initials').each(function() {
              const firstName = $(this).data('firstname');
              const lastName = $(this).data('lastname');
              const initials = (firstName ? firstName.charAt(0) : '') + (lastName ? lastName.charAt(0) : '');
              $(this).text(initials);
          });
      });
  });
  return (
    <>
    <style>{cssStyles}</style>
      <link
        rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css"
        integrity="sha256-3sPp8BkKUE7QyPSl6VfBByBroQbKxKG7tsusY2mhbVY="
        crossorigin="anonymous"
      />
      <div className="container">
        
        <div className="row">
          <div className="col-lg-10 mx-auto">
            <div className="career-search mb-60">
              <form action="#" className="career-form mb-60">
                <div className="row">
                  <div className="col-md-6 col-lg-3 my-3">
                    <div className="input-group position-relative">
                      <input type="text" className="form-control" placeholder="Enter Your Keywords" id="keywords" />
                    </div>
                  </div>
                  <div className="col-md-6 col-lg-3 my-3">
                    <div className="select-container">
                      <select className="custom-select">
                        <option selected="">adress</option>
                      
                      </select>
                    </div>
                  </div>
                  <div className="col-md-6 col-lg-3 my-3">
                    <div className="select-container">
                      <select className="custom-select">
                        <option selected="">Select specialty</option>
                       
                      </select>
                    </div>
                  </div>
                  <div className="col-md-6 col-lg-3 my-3">
                    <button type="button" className="btn btn-lg btn-block btn-light btn-custom" id="contact-submit">
                      Search
                    </button>
                  </div>
                </div>
              </form>

              <div className="filter-result">
                <p className="mb-30 ff-montserrat">Total : {userData.length}</p>

                {userData.map((user) => (
                  <div className="job-box d-md-flex align-items-center justify-content-between mb-30" key={user.id}>
                    <div className="job-left my-4 d-md-flex align-items-center flex-wrap">
                    <div className="img-holder mr-md-4 mb-md-0 mb-4 mx-auto mx-md-0 d-md-none d-lg-flex profile-initials"
                     data-firstname={user.firstname}
                       data-lastname={user.lastname}>
                                        </div>
                      
                      <div className="job-content">
                        <h5 className="text-center text-md-left">{user.firstname} {user.lastname}</h5>
                        <ul className="d-md-flex flex-wrap text-capitalize ff-open-sans">
                          <li className="mr-md-4">
                            <i className="zmdi zmdi-pin mr-2"></i> {user.adress}
                          </li>
                          <li className="mr-md-4">
                            <i className="zmdi zmdi-time mr-2"></i> {user.specialityy}
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div className="job-right my-4 flex-shrink-0">
                      <button className="btn d-block w-100 d-sm-inline-block btn-light" onClick={() => handleContactClick(user.id)}>
                        Contact
                      </button>
                    </div>
                  </div>
                ))}

              </div>
            </div>

            <nav aria-label="Page navigation">
              <ul className="pagination pagination-reset justify-content-center">
                <li className="page-item disabled">
                  <a className="page-link" href="#" tabIndex="-1" aria-disabled="true">
                    <i className="zmdi zmdi-long-arrow-left"></i>
                  </a>
                </li>
                <li className="page-item"><a className="page-link" href="#">1</a></li>
                <li className="page-item d-none d-md-inline-block"><a className="page-link" href="#">2</a></li>
                <li className="page-item d-none d-md-inline-block"><a className="page-link" href="#">3</a></li>
                <li className="page-item"><a className="page-link" href="#">...</a></li>
                <li className="page-item"><a className="page-link" href="#">8</a></li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    <i className="zmdi zmdi-long-arrow-right"></i>
                  </a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </>
  );
}

export default Calls;
