import React, { useState } from 'react';
import axios from 'axios';
import { useLocation, useNavigate } from 'react-router-dom';
import 'react-phone-number-input/style.css';
import PhoneInput from 'react-phone-number-input';
import photo from '../Assets/1.png';

import logoLeft from '../Assets/Fichier 7 (1).png'; // Importez votre logo à gauche

import {
  MDBBtn,
  MDBContainer,
  MDBCard,
  MDBCardBody,
  MDBInput,
  MDBRow,
  MDBCol
} from 'mdb-react-ui-kit';

function SignupClient() {
  const navigate = useNavigate();
  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);
  const role = queryParams.get('role');
  const [firstname, setFirstname] = useState('');
  const [lastname, setLastname] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [adress, setAdress] = useState('');
  const [businessName, setBusinessName] = useState('');
  const [specialty, setSpecialty] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [errors, setErrors] = useState({});

  const saveClient = async (event) => {
    event.preventDefault();

    const newErrors = {};

    // Vérification des champs requis
    if (!firstname) newErrors.firstname = 'Please enter your first name';
    if (!lastname) newErrors.lastname = 'Please enter your last name';
    if (!email) newErrors.email = 'Please enter your email';
    if (!password) newErrors.password = 'Please enter your password';
    if (!adress) newErrors.adress = 'Please enter your address';
    if (!businessName) newErrors.businessName = 'Please enter your business name';
    if (!specialty) newErrors.specialty = 'Please enter your specialty';
    if (!phoneNumber) newErrors.phoneNumber = 'Please enter your phone number';

    // Vérification du format de l'email
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (email && !emailRegex.test(email)) {
      newErrors.email = 'Please enter a valid email address';
    }

    // Vérification du mot de passe
    const passwordRegex = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/;
    if (password && !passwordRegex.test(password)) {
      newErrors.password = 'Password must contain at least 8 characters, one letter, one number, and one special character';
    }

    if (Object.keys(newErrors).length > 0) {
      setErrors(newErrors);
      return;
    }

    try {
      await axios.post('http://localhost:8080/api/v1/auth/register', {
        firstname: firstname,
        lastname: lastname,
        email: email,
        password: password,
        adress: adress,
        businessName: businessName,
        phoneNumber:phoneNumber,
        specialty: specialty,
        role: role || 'CLIENT'
      });
      navigate("/login");
    } catch (error) {
      setErrors({ apiError: 'Registration failed. Please try again.' });
    }
  };

  const cssStyles = `
  .cascading-right {
    margin-right: -40px;
  }
  
  @media (max-width: 900px) {
    .cascading-right {
      margin-right: 1;
    }
  }
  .logo {
    position: absolute;
    top: -100px;
    left: 50px;
    width: 200px; /* Adjust the size of the logo */
    height: auto;
    z-index: 1;
  }
  `;

  return (
    <>
      <style>{cssStyles}</style>
      <div style={{ position: 'absolute', top: '200px', left: '300px' }}>
        <img className="logo" src={logoLeft} alt="Logo" />
      </div>
      <MDBContainer fluid className='my-2'>
        <MDBRow className='g-3 align-items-center justify-content-center' style={{ minHeight: '100vh' }}>
          <MDBCol md='5'>
            <MDBCard className='my-5 cascading-right' style={{ background: 'hsla(0, 0%, 100%, 0.55)', backdropFilter: 'blur(30px)' }}>
              <MDBCardBody className='p-5 shadow-5 text-center'>
                <h2 className="fw-bold mb-5"></h2>
                <MDBRow>
                  <MDBCol col='6'>
                    <MDBInput wrapperClass='mb-4' label='FirstName' id='form1' type='text' value={firstname} onChange={(e) => setFirstname(e.target.value)} />
                    {errors.firstname && <div className="text-danger">{errors.firstname}</div>}
                  </MDBCol>
                  <MDBCol col='6'>
                    <MDBInput wrapperClass='mb-4' label='LastName' id='form2' type='text' value={lastname} onChange={(e) => setLastname(e.target.value)} />
                    {errors.lastname && <div className="text-danger">{errors.lastname}</div>}
                  </MDBCol>
                </MDBRow>
                <MDBInput wrapperClass='mb-4' label='Email' id='form3' type='email' value={email} onChange={(e) => setEmail(e.target.value)} />
                {errors.email && <div className="text-danger">{errors.email}</div>}
                <MDBInput wrapperClass='mb-4' label='Password' id='form4' type='password' value={password} onChange={(e) => setPassword(e.target.value)} />
                {errors.password && <div className="text-danger">{errors.password}</div>}
                <MDBInput wrapperClass='mb-4' label='Adress' id='form5' type='text' value={adress} onChange={(e) => setAdress(e.target.value)} />
                {errors.adress && <div className="text-danger">{errors.adress}</div>}
                <MDBInput wrapperClass='mb-4' label='Business Name' id='form6' type='text' value={businessName} onChange={(e) => setBusinessName(e.target.value)} />
                {errors.businessName && <div className="text-danger">{errors.businessName}</div>}
                <MDBInput wrapperClass='mb-4' label='Post' id='form7' type='text' value={specialty} onChange={(e) => setSpecialty(e.target.value)} />
                {errors.specialty && <div className="text-danger">{errors.specialty}</div>}
                <PhoneInput
                  placeholder="Enter phone number"
                  value={phoneNumber}
                  onChange={setPhoneNumber} />
                {errors.phoneNumber && <div className="text-danger">{errors.phoneNumber}</div>}
                <div className='d-flex justify-content-center mb-4'>
                </div>
                <MDBBtn className='w-100 mb-4' size='md' onClick={saveClient}>Sign up</MDBBtn>
                {errors.apiError && <div className="text-danger">{errors.apiError}</div>}
                <div className="text-center"></div>
              </MDBCardBody>
            </MDBCard>
          </MDBCol>
          <MDBCol md='6'>
            <img src={photo} className="w-100 rounded-4 shadow-4" alt="" fluid />
          </MDBCol>
        </MDBRow>
      </MDBContainer>
    </>
  );
}

export default SignupClient;
