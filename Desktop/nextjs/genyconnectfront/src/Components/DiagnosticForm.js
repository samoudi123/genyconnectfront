import React from "react";
import { useState } from "react";
import axios from "axios";
import { useNavigate, useParams } from "react-router-dom";
function DiagnosticForm(){
  const navigate = useNavigate(); // Initialisation de useNavigate
const {reference} = useParams();
  
  const [cost, setCost] = useState('');
  const [duration, setDuration] = useState('');
  const [problems, setProblems] = useState('');
  const [causes, setCauses] = useState('');


    const saveForm = async (event) => {
    
        event.preventDefault();
        try {
          await axios.put(`http://localhost:8080/api/request/updateRequest/${reference}`, {
            cost:cost,
            duration:duration,
            problems:problems,
            causes:causes
          
          });
          alert('  Successfull saving ');
          navigate(`/diagnosticFiche?cost=${cost}&duration=${duration}&problems=${problems}&causes=${causes}`);

        } catch (error) {
          alert(error);

        }
      };



    return(

        <section class="bg-light py-3 py-md-5">
          <div class="container">
            <div class="row justify-content-md-center">
              
            </div>
          </div>
        
          <div class="container">
            <div class="row justify-content-lg-center">
              <div class="col-12 col-lg-9">
                <div class="bg-white border rounded shadow-sm overflow-hidden">
        
                  <form action="#!">
                    <div class="row gy-4 gy-xl-5 p-4 p-xl-5">
                      
                      <div class="col-12 col-md-6">
                        <label for="email" class="form-label">Intervention cost <span class="text-danger">*</span></label>
                        <div class="input-group">
                          <span class="input-group-text">
                          </span>
                          <input type="text" class="form-control"  name=" cost"  required placeholder="Intervention cost"
                           value={cost} onChange={(e) => setCost(e.target.value)}
                           inputMode="numeric" // Ajout de l'attribut inputMode

                           />
                          <span style={{ position: 'absolute', top: '50%', right: '10px', transform: 'translateY(-50%)', color: '#495057' }}>$</span>

                        </div>

                      </div>
                      <div class="col-12 col-md-6">
                        <label for="phone" class="form-label">Duration of intervention<span class="text-danger">*</span></label>
                        <div class="input-group">
                          <span class="input-group-text">
                              <path d="M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z" />
                          </span>
                          <input type="text" class="form-control" id="duration" name="Duration"  placeholder="Duration of intervention"
                                                     value={duration} onChange={(e) => setDuration(e.target.value)}
                                                     />
                          <span style={{ position: 'absolute', top: '50%', right: '10px', transform: 'translateY(-50%)', color: '#495057' }}>day</span>

                        </div>
                      </div>
                      <div class="col-12">
                        <label for="message" class="form-label">the causes of the breakdown
                         <span class="text-danger">*</span></label>
                        <input type="text" className="form-control" required
                        value={causes} onChange={(e) => setCauses(e.target.value)}/>

                              
                      </div>
                      <div class="col-12">
                        <label for="message" class="form-label">Problems
                         <span class="text-danger">*</span></label>
                        <select id="inputState" className="form-select" required
                                                   value={problems} onChange={(e) => setProblems(e.target.value)}
                                                   >
                                <option selected>Choose...</option>
                                <option>Mechanical problems,Component wear, software problems,Software bugs</option>
                               

                            </select>
                      </div>
                      <div class="col-12">
                        <div class="d-grid">
                          <button class="btn btn-primary btn-lg" type="submit" onClick={saveForm}>Next</button>
                        </div>
                      </div>
                    </div>
                  </form>
        
                </div>
              </div>
            </div>
          </div>
        </section>
    )
}
export default DiagnosticForm ; 