import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import photo from '../Assets/1.png';
import { Link } from 'react-router-dom';
import logoLeft from '../Assets/Fichier 7 (1).png'; // Importez votre logo à gauche
import {
  MDBBtn,
  MDBContainer,
  MDBCard,
  MDBCardBody,
  MDBCardImage,
  MDBRow,
  MDBCol,
  MDBInput,
  MDBCheckbox
}
from 'mdb-react-ui-kit';
import axios from 'axios';

function Login() {
   
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const navigate = useNavigate();

    const handleEmail = (e) => {
        setEmail(e.target.value);
    }

    const handlePassword = (e) => {
        setPassword(e.target.value);
    }

    const handleApi = () => {
        console.log({ email, password });
        axios.post("http://localhost:8080/api/v1/auth/authenticate", {
            email: email,
            password: password,
        })
        .then(result => {
            console.log(result.data);
            const accessToken = result.data.accessToken;
            const refreshToken = result.data.refreshToken;
            const user = result.data.user; 
            const userId = user.id;
            const userRole = user.role;
        const roleName = userRole.split(',')[1].split('=')[1].replace(')', '').trim();
            console.log(userId)

            if (accessToken && refreshToken) {
                localStorage.setItem('accessToken', accessToken);
                localStorage.setItem('refreshToken', refreshToken);
                localStorage.setItem('currentUser', userId);
                console.log("CurrentUser:", userId);

                console.log("Access token:", accessToken);
                console.log("Refresh token:", refreshToken);

                console.log(result.data)
                switch (roleName){
                    case'ADMIN':
                    navigate('/users');
                    break;
                    case 'CLIENT':
                        navigate(`/profileClient/${userId}`);
                        break; 
                        case 'TECHNICIAN':
                        navigate(`/ProfileTechnician/${userId}`);
                        break ; 
                        case 'ENTREPRISE':
                            navigate(`/ProfileEntreprise/${userId}`);
                            break;


                }
            } else {
                console.error("Tokens are missing in the response");
                // Gérer le cas où les tokens sont manquants
            }
        })
        .catch(error => {
            console.error("Error occurred while authenticating:", error);
            alert('Erreur lors de l\'authentification. Veuillez réessayer.');
        });
    }

    // Styles CSS
    const cssStyles = `
        .rounded-t-5 {
            border-top-left-radius: 0.5rem;
            border-bottom-left-radius: 0.5rem;
        }
        
        @media (max-width: 550px) {
            .rounded-tr-lg-0 {
                border-top-right-radius: 0.5rem;
                border-bottom-left-radius: 0rem;
            }
        }
        
        .logo {
            position: fixed;
            top: 70px;
            left: 800px;
            width: 200px;
            height: auto;
            z-index: 1000;
        }
    `;

    return (
        <>
            <style>{cssStyles}</style>
            <div style={{ position: 'absolute', top: '100px', left: '850px' }}>
                <img className="logo" src={logoLeft} alt="Logo" />
            </div>
            
            <MDBContainer className='my-5'>
                <MDBCard>
                    <MDBRow className='g-0 d-flex align-items-center'>
                        <MDBCol md='4'>
                            <MDBCardImage src={photo} alt='phone' className='rounded-t-5 rounded-tr-lg-0' fluid />
                        </MDBCol>
                        <MDBCol md='8'>
                            <MDBCardBody>
                                <MDBInput wrapperClass='mb-4' label='Email address' id='form1' type='email' value={email} onChange={handleEmail} />
                                <MDBInput wrapperClass='mb-4' label='Password' id='form2' type='password' value={password} onChange={handlePassword} />
                                <div className="d-flex justify-content-between mx-4 mb-4">
                                    <Link to='/roleChoice' >
                                <a href="!#">Don't have an account?</a>
                                </Link>

                                    <a href="!#">Forgot password?</a>
                                </div>
                                <MDBBtn className="mb-4 w-100" onClick={handleApi}>Sign in</MDBBtn>
                            </MDBCardBody>
                        </MDBCol>
                    </MDBRow>
                </MDBCard>
            </MDBContainer>
        </>
    );
}

export default Login;
