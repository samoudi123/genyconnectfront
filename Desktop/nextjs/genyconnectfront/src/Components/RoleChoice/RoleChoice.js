import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import logoLeft from '../Assets/Fichier 7 (1).png';
import backgroundImage from '../Assets/30758.jpg';
import logoClient from '../Assets/client.png';
import logoEntreprise from '../Assets/entreprise.png';
import logoTechnician from '../Assets/tech.png';
import { Link } from 'react-router-dom';

function RoleChoice() {
  const [selectedLogo, setSelectedLogo] = useState(null);
  const [registrationStarted, setRegistrationStarted] = useState(false);
  const navigate = useNavigate();

  const handleLogoClick = (role) => {
    setSelectedLogo(role);
    setRegistrationStarted(true);
  };

  const handleStartRegistration = () => {
    if (selectedLogo === 'Client') {
      navigate('/signupClient?role=client');
    }
    if (selectedLogo === 'Entreprise') {
      navigate('/signupEntreprise?role=entreprise');
    }
    if (selectedLogo === 'Technician') {
      navigate('/SignUpTechnician?role=technician');
    }
  };

  // Styles CSS
  const cssStyles = `
    /* Styles for the blank page */
    .blank-page {
      position: relative;
      width: 100vw;
      height: 100vh;
      overflow: hidden;
    }
    .logo1 {
      width: 200px;
  height: 60px;
  position: absolute;
  top: 150px;
  left: 230px;
  z-index: 10;
    }
    .background-image-container1 {
      position: absolute;
      top: 0;
      right: 0;
      bottom: 0;
      left: 20%;
      overflow: hidden;
    }
    .background-image {
      position: absolute;
      top: 0;
      right: 0;
      left: 0;
      width: 60%;
      height: 100%;
      transform: translateX(50%);
      object-fit: cover;
    }
    .main-title {
      font-size: 40px;
      text-decoration-color: darkgray;
    }
    .sub-title {
      font-size: 24px;
    }
    .titles-container {
      position: absolute;
      top: 35%;
      left: 25%;
      transform: translate(-50%, -50%);
      text-align: left;
      z-index: 1;
    }
    .logos-container {
      display: flex;
      justify-content: center;
      margin-top: 100px;
      position: absolute;
      top: 45%;
      left: 25%;
      transform: translate(-50%, -50%);
      text-align: left;
      z-index: 1;
    }
    .logo-item {
      text-align: center;
      margin: 0 30px;
      padding: 5px;
    }
    .logo-item img {
      width: 100px;
      height: auto;
    }
    .custom-button {
      padding: 10px 200px;
      background-color: #007bff;
      color: white;
      border: none;
      border-radius: 5px;
      cursor: pointer;
      margin-top: 20px;
      display: flex;
      justify-content: center;
      margin-top: 230px;
      position: absolute;
      top: 45%;
      left: 25%;
      transform: translate(-50%, -50%);
      text-align: left;
      z-index: 1;
    }
    .link-text {
      color: #007bff;
      margin-top: 20px;
      padding: 10px 200px;
      color: white;
      border: none;
      border-radius: 5px;
      cursor: pointer;
      margin-top: 20px;
      display: flex;
      justify-content: center;
      margin-top: 230px;
      position: absolute;
      top: 50%;
      left: 25%;
      transform: translate(-50%, -50%);
      text-align: left;
      z-index: 1;
    }
    .link-text a {
      text-decoration: none;
      color: black;
    }
    .link-text a:hover {
      text-decoration: underline;
    }
    .custom-button.disabled {
      opacity: 0.5;
      pointer-events: none;
    }
  `;

  return (
    <div className="blank-page">
      <style>{cssStyles}</style>
      <img className="logo1" src={logoLeft} alt="Logo" />
      <div className="titles-container">
        <h1 className='main-title'>Register?</h1>
        <h2 className='sub-title'>To start, choose your profile</h2>
      </div>
      <div className="logos-container">
        <div className="logo-item">
          <button
            className={`logo-button ${selectedLogo === 'Entreprise' ? 'selected' : ''}`}
            onClick={() => handleLogoClick('Entreprise')}
          >
            <img src={logoEntreprise} alt="Logo Entreprise" />
            <p>Entreprise</p>
          </button>
        </div>
        <div className="logo-item">
          <button
            className={`logo-button ${selectedLogo === 'Client' ? 'selected' : ''}`}
            onClick={() => handleLogoClick('Client')}
          >
            <img src={logoClient} alt="Logo Client" />
            <p>Client</p>
          </button>
        </div>
        <div className="logo-item">
          <button
            className={`logo-button ${selectedLogo === 'Technician' ? 'selected' : ''}`}
            onClick={() => handleLogoClick('Technician')}
          >
            <img src={logoTechnician} alt="Logo Technician" />
            <p>Technician</p>
          </button>
        </div>
      </div>
      <button
        className={`custom-button ${selectedLogo ? '' : 'disabled'}`}
        onClick={handleStartRegistration}
      >
        Start Registration
      </button>
      <p className="link-text">
        <Link to="/login">Already have an account?</Link>
      </p>
      <div className="background-image-container1">
        <img className="background-image" src={backgroundImage} alt="Background" />
      </div>
    </div>
  );
}

export default RoleChoice;
