import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

const ListUsers = () => {
    const [users, setUsers] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await axios.get(`http://localhost:8080/api/v1/users`);
                setUsers(response.data);
            } catch (error) {
                console.error("Error fetching users: ", error);
            }
        };

        fetchData();
    }, []);

    const deleteUsers = async (userId) => {
        try {
            await axios.delete(`http://localhost:8080/api/v1/users/delete/${userId}`);
            // Rafraîchir la liste des utilisateurs après la suppression
            const updatedUsers = users.filter(user => user.id !== userId);
            setUsers(updatedUsers);
        } catch (error) {
            console.error("Error deleting user: ", error);
        }
    };

    return (
        <div className="container">
            <h2 className="text-center">List Users</h2>
            <Link to="/adduser" className="btn btn-outline-primary mb-2">
                Add User
            </Link>
            <table className="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>User Id</th>
                        <th>User FirstName</th>
                        <th>User LastName</th>
                        <th>User Email</th>
                        <th>User role</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {users.map(user => (
                        <tr key={user.id}>
                            <td>{user.id}</td>
                            <td>{user.firstname}</td>
                            <td>{user.lastname}</td>
                            <td>{user.email}</td>
                            <td>{user.role}</td>
                            <td>
                                <Link to={`/updateuser/${user.id}`} className="btn btn-info">
                                    Update
                                </Link>
                                <button
                                    className="btn btn-outline-info"
                                    style={{ marginLeft: '10px' }}
                                    onClick={() => deleteUsers(user.id)} // Appel à deleteUsers avec l'ID de l'utilisateur
                                >
                                    Delete
                                </button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
};

export default ListUsers;
