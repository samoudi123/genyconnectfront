import React, {useState, useEffect} from 'react'
import {Link, useNavigate, useParams } from 'react-router-dom';
import UserService from './adminService';
import axios from 'axios';
const AddUserService = () => {

    const [firstname, setFirstname] = useState('')
    const [lastname, setLastname] = useState('')
    const [email, setEmail] = useState('')
    const [role, setRole] = useState('')
    const [password, setPassword] = useState('')


    const navigate = useNavigate();
    const {id} = useParams();

    const saveOrUpdateUser = (e) => {
        e.preventDefault();

        const user = {firstname, lastname, email,password}

        if(id){
            UserService.updateUser(id, user).then((response) => {
                navigate('/employees')
            }).catch(error => {
                console.log(error)
            })

        }else{
            UserService.createUser(user).then((response) =>{

                console.log(response.data)
    
                navigate('/employees');
    
            }).catch(error => {
                console.log(error)
            })
        }
        
    }

    useEffect(() => {

        UserService.getUserById(id).then((response) =>{
            setFirstname(response.data.firstname)
            setLastname(response.data.lastname)
            setEmail(response.data.email)
            setPassword(response.data.password)
        }).catch(error => {
            console.log(error)
        })
    }, [])

    const title = () => {

        if(id){
            return <h2 className = "text-center">Update User</h2>
        }else{
            return <h2 className = "text-center">Add User</h2>
        }
    }
    const saveUser= async (event) =>{
    
        event.preventDefault();
        try {
          await axios.post('http://localhost:8080/api/v1/users/addUser', {
            firstname:firstname,
            lastname:lastname,
            email:email,
            role:role,
            password:password
          
          });
          alert('saving');

        } catch (error) {
          alert(error);

        }
      };

    return (
        <div>
           <br /><br />
           <div className = "container">
                <div className = "row">
                    <div className = "card col-md-6 offset-md-3 offset-md-3">
                       {
                           title()
                       }
                        <div className = "card-body">
                            <form>
                                <div className = "form-group mb-2">
                                    <label className = "form-label"> FirstName :</label>
                                    <input
                                        type = "text"
                                        placeholder = "Enter firstname"
                                        name = "firstName"
                                        className = "form-control"
                                        value = {firstname}
                                        onChange = {(e) => setFirstname(e.target.value)}
                                    >
                                    </input>
                                </div>

                                <div className = "form-group mb-2">
                                    <label className = "form-label"> LastName :</label>
                                    <input
                                        type = "text"
                                        placeholder = "Enter lastname"
                                        name = "lastName"
                                        className = "form-control"
                                        value = {lastname}
                                        onChange = {(e) => setLastname(e.target.value)}
                                    >
                                    </input>
                                </div>
                                <div className = "form-group mb-2">
                                    <label className = "form-label"> Role :</label>
                                    <input
                                        type = "text"
                                        placeholder = "Enter role "
                                        name = "role"
                                        className = "form-control"
                                        value = {role}
                                        onChange = {(e) => setRole(e.target.value)}
                                    >
                                        
                                    </input>
                                </div>

                                <div className = "form-group mb-2">
                                    <label className = "form-label"> Email :</label>
                                    <input
                                        type = "email"
                                        placeholder = "Enter email "
                                        name = "email"
                                        className = "form-control"
                                        value = {email}
                                        onChange = {(e) => setEmail(e.target.value)}
                                    >
                                        
                                    </input>
                                </div>
                                <div className = "form-group mb-2">
                                    <label className = "form-label"> Password :</label>
                                    <input
                                        type = "password"
                                        placeholder = "Enter Password "
                                        name = "password"
                                        className = "form-control"
                                        value = {password}
                                        onChange = {(e) => setPassword(e.target.value)}
                                    >
                                        
                                    </input>
                                </div>
                            

                                <button class="btn btn-outline-success" onClick={saveUser} >Submit </button>
                                <Link to="/users" class="btn btn-outline-success m-2"> Cancel </Link>
                            </form>

                        </div>
                    </div>
                </div>

           </div>

        </div>
    )
}

export default AddUserService