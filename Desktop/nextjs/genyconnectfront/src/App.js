import React from 'react';

import Login from './Components/Login/Login';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import RoleChoice from './Components/RoleChoice/RoleChoice';
import SignupClient from './Components/SignupClient/SignupClient';
import SignupEntreprise from './Components/SignupEntreprise/SignupEntreprise';
import SignupTechnician from './Components/SignupTechnician/SignupTechnician';
import ListUsers from './Components/Admin/admin';
import AddUserService from './Components/Admin/addUserService';
import Home from './Components/Home/Home';
import ProfileClient from './Components/ProfileClient/ProfileClient';
import ProfileEntreprise from './Components/ProfileEntreprise/ProfileEntreprise';
import ProfileTechnician from './Components/ProfileTechnician/ProfileTechnician';
import InterventionForm from './Components/Intervention/interventioForm';
import Intervention from './Components/Intervention/intevention';
import FicheIntervention from './Components/Intervention/ficheIntervention';
import UploadFiche from './Components/Intervention/UploadFiche';
import Profiles from './Components/Intervention/profiles';
import InterventionTech from './Components/interventionTech';
import Profile from './Components/Intervention/profile';
import DetailsIntervention from './Components/Intervention/DetailsIntervention';
import ChatRoom from './Components/Chat/ChatRoom';
import SideBar1 from './Components/profile/SideBar';

import DetailsFiche from './Components/DetailsFiche';
import DiagnosticForm from './Components/DiagnosticForm';
import FicheDiagnostic from './Components/Intervention/ficheDiagnostic';
import UploadDiagnostic from './Components/Intervention/UploadFicheD';
import Diagnostic from './Components/Intervention/UploadFicheD';
import Step from './Components/progress/step';
import Payment from './Components/progress/payment';
import StepTech from './Components/progress/stepTech';
import Home1 from './Components/VideoCall/Home1';
import Room from './Components/VideoCall/Room';
import Calls from './Components/Chat/calls';
import FeedbackForm from './Components/FeedBack/form';
import Submissions from './Components/FeedBack/submissions';
import Submissions1 from './Components/FeedBack/submission1';
import FeedbackForm1 from './Components/FeedBack/form1';

function App() {
  return (
    <div>
      <Router>

        <div className="container">
          <Routes>
            <Route path="/RoleChoice" element={<RoleChoice />} />
            <Route path="/" element={<Home />} />
            <Route path="/video" element={<Home1 />} />
            <Route path="/room/:roomID" element={<Room />} />
            <Route path="/form/:userId" element={<FeedbackForm />} />
            <Route path="/submissions/:userId" element={<Submissions />} />
            <Route path="/form1/:userId" element={<FeedbackForm1 />} />
            <Route path="/submissions1/:userId" element={<Submissions1 />} />







            <Route path="/signupClient" element={<SignupClient />} />

            <Route path="/ProfileClient/:userId" element={ <div>  <SideBar1 />   <ProfileClient /></div> } />  
            <Route path="/intervention/:userId" element={ <Intervention />}/>  

            <Route path="/ProfileEntreprise/:userId" element={<ProfileEntreprise />} />
            <Route path="/ProfileTechnician/:userId" element={<ProfileTechnician />} />
            <Route path="/interventionForm/:userId" element={<InterventionForm />} />


            <Route path="/chat" element={<ChatRoom />} />
            <Route path="/calls" element={<Calls />} />




            <Route path="/signupTechnician" element={<SignupTechnician />} />
            <Route path="/signupEntreprise" element={<SignupEntreprise />} />
            <Route path="/users" element={<ListUsers />} />
            <Route path="/login" element={<Login />} />
            <Route path="/adduser" element={<AddUserService />} />
            <Route path="/interventionTech/:userId" element={<InterventionTech />} />
            <Route path="/profile/:userId/:id" element={<Profile />} />
            <Route path="/details/:reference" element={<DetailsIntervention />} />
            <Route path="/fiche" element={<FicheIntervention />} />
            <Route path="/uploadfiche/:id" element={<UploadFiche />} />

            <Route path="/profiles/:id" element={<Profiles />} />

            <Route path="/detaills/:reference" element={<DetailsFiche />} />
            <Route path="/diagnostic/:reference" element={<DiagnosticForm />} />
            <Route path="/diagnosticFiche" element={<FicheDiagnostic />} />
            <Route path="/uploadD/:id" element={<Diagnostic />} />
            <Route path="/timeline/:reference" element={<Step />} />
            <Route path="/payment" element={<Payment />} />
            <Route path="/timeline1/:reference" element={<StepTech />} />








          </Routes>
        </div>
      </Router>
    </div>
    
      
  );
}
      export default App;
    
